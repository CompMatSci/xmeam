# ------------------------ FORCE FIELDS ---------------------------------------
# XMEAM potential for Nb 2022 
# -----------------------------------------------------------------------------

variable ref_doi string 'notapplicable'
variable ref     string 'wang_xmeam_8'
pair_style      xmeam
pair_coeff      * * ${lmp_t_dir}/potential/Nb/wang_xmeam_8/library.xmeam Nb ${lmp_t_dir}/potential/Nb/wang_xmeam_8/Nb.xmeam Nb

neighbor        0.3 bin
neigh_modify     delay 10

variable Nb equal 1
mass ${Nb} 92.9060
