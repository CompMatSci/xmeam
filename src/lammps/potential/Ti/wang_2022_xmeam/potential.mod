# ------------------------ FORCE FIELDS ---------------------------------------
# XMEAM potential for Ti 2022
# -----------------------------------------------------------------------------
variable ref_doi string 'notapplicable'
variable ref     string 'notapplicable'
pair_style      xmeam
pair_coeff      * * ${lmp_t_dir}/potential/Ti/wang_2022_xmeam/library.xmeam Ti ${lmp_t_dir}/potential/Ti/wang_2022_xmeam/Ti.xmeam Ti
#pair_style      deepmd frozen_model.pb
#pair_coeff * *
neighbor        0.3 bin
neigh_modify     delay 10
variable Ti equal 1
mass ${Ti} 47.867
