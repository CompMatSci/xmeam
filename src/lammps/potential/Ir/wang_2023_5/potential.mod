# ------------------------ FORCE FIELDS ---------------------------------------
# XMEAM potential for Ir 2023 
# -----------------------------------------------------------------------------
variable ref_doi string 'notapplicable'
variable ref     string 'wang_2023_5'
pair_style      xmeam
pair_coeff      * * ${lmp_t_dir}/potential/Ir/wang_2023_5/library.xmeam Ir ${lmp_t_dir}/potential/Ir/wang_2023_5/Ir.xmeam Ir
neighbor        0.3 bin
neigh_modify     delay 10
variable Ir equal 1
mass ${Ir} 192.217
