# ------------------------ FORCE FIELDS ---------------------------------------
# XMEAM potential for Al 2023 
# -----------------------------------------------------------------------------
variable ref_doi string 'notapplicable'
variable ref     string 'wang_2023_xmeam'
pair_style      xmeam
pair_coeff      * * ${lmp_t_dir}/potential/Al/wang_2023_xmeam/library.xmeam Al ${lmp_t_dir}/potential/Al/wang_2023_xmeam/Al.xmeam Al
neighbor        0.3 bin
neigh_modify     delay 10
variable Al equal 1
mass ${Al} 26.98
