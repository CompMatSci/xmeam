# ------------------------ FORCE FIELDS ---------------------------------------
# XMEAM potential for Mo 2023 
# -----------------------------------------------------------------------------

variable ref_doi string 'notapplicable'
variable ref     string 'wang_2023_4'
pair_style      xmeam
pair_coeff      * * ${lmp_t_dir}/potential/Mo/wang_2023_4/library.xmeam Mo ${lmp_t_dir}/potential/Mo/wang_2023_4/Mo.xmeam Mo

neighbor        0.3 bin
neigh_modify     delay 10

variable Mo equal 1
mass ${Mo} 95.95

