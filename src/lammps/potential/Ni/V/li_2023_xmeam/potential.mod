# ------------------------ FORCE FIELDS ------------------------------
# NiV XMEAM potential by Li Mengwan et al.
# --------------------------------------------------------------------
variable ref_doi string 'not published'
variable ref     string 'not published'
pair_style      xmeam
pair_coeff      * * ${pot_prefix}/library.xmeam Ni V ${pot_prefix}/NiV.xmeam Ni V
neighbor        0.3 bin
neigh_modify    delay 10
variable Ni equal 1
variable V equal 2

