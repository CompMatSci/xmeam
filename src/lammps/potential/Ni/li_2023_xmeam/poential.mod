# ------------------------ FORCE FIELDS ------------------------------
# Ni XMEAM potential by Li_2023_XMEAM, not published
# --------------------------------------------------------------------
pair_style      xmeam
pair_coeff       * * ${lmp_t_dir}/potential/Ni/li_2023_xmeam/Ni_library.xmeam Ni ${lmp_t_dir}/potential/Ni/li_2023_xmeam/Ni.xmeam Ni
     
neighbor        0.3 bin
neigh_modify    delay 10
variable Ni equal 1
