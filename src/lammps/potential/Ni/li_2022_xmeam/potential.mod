# ------------------------ FORCE FIELDS ------------------------------
# Ni XMEAM potential by Li_2022_XMEAM, not published
# --------------------------------------------------------------------
variable ref_doi string 'to be updated'
variable ref     string 'to be updated'
pair_style      xmeam
pair_coeff       * * ${lmp_t_dir}/potential/Ni/li_2022_xmeam/Ni_library.xmeam Ni ${lmp_t_dir}/potential/Ni/li_2022_xmeam/Ni.xmeam Ni
     
neighbor        0.3 bin
neigh_modify    delay 10
variable        Ni equal 1
