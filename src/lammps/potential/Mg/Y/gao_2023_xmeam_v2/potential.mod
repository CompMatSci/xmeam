# ------------------------ FORCE FIELDS ------------------------------
# MgY XMEAM potential by Rui and Gao 2023
# --------------------------------------------------------------------

variable ref_doi string 'not published'
variable ref string 'gao_2023_xmeam_v2'

pair_style      xmeam
pair_coeff      * * ${lmp_t_dir}/potential/Mg/Y/gao_2023_xmeam_v2/library.xmeam Mg Y ${lmp_t_dir}/potential/Mg/Y/gao_2023_xmeam_v2/MgY.xmeam Mg Y

neighbor        0.3 bin
#neigh_modify     delay 10
neigh_modify every 1 delay 0 check yes

variable Mg equal 1
variable Y equal 2

mass ${Mg} 24.320
mass ${Y}  88.90584
