# ------------------------ FORCE FIELDS ------------------------------
# Mg XMEAM potential by R. Wang 2022
# --------------------------------------------------------------------

variable ref_doi string 'not published'
variable ref string 'wang_2022_11'

pair_style      xmeam
pair_coeff      * * ${lmp_t_dir}/potential/Mg/wang_2022_11/library.xmeam Mg ${lmp_t_dir}/potential/Mg/wang_2022_11/Mg.xmeam Mg

neighbor        0.3 bin
neigh_modify     delay 10

variable Mg equal 1
