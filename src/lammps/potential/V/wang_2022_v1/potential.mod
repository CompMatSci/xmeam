# ------------------------ FORCE FIELDS ---------------------------------------
# XMEAM potential for V 2022 V1
# -----------------------------------------------------------------------------

variable ref_doi string 'notapplicable'
variable ref     string 'wang_2022_v1'
pair_style      xmeam
pair_coeff      * * ${lmp_t_dir}/potential/V/wang_2022_v1/library.xmeam V ${lmp_t_dir}/potential/V/wang_2022_v1/V.xmeam V

neighbor        0.3 bin
neigh_modify     delay 10

variable V equal 1
mass ${V} 50.942
