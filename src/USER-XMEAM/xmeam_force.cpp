#include "xmeam.h"
#include <cmath>
#include <algorithm>
#include "math_special.h"

using namespace LAMMPS_NS;

void
XMEAM::xmeam_force(int i, int eflag_either, int eflag_global, int eflag_atom,
                   int vflag_atom, double* eng_vdwl, double* eatom, int ,
                   int* type, int* fmap, double** x, int numneigh,
                   int* firstneigh, int numneigh_full,
                   int* firstneigh_full, int fnoffset,
                   double** f, double** vatom) {
  int j, jn, k, kn, kk, m, n, o, p, q;
  int nv2, nv3, nv4, nv5, elti, eltj, eltk, ind;
  double xitmp, yitmp, zitmp, delij[3], rij2, rij, rij3, rij4, rij5;
  double v[6], fi[3], fj[3];
  double third, sixth;
  double pp, dUdrij, dUdsij, dUdrijm[3], force, forcem;
  double r, recip, phi, phip;
  double sij;
  double a1, a1i, a1j, a2, a2i, a2j;
  double a3i, a3j, a4i, a4j, a5i, a5j;
  double shpi[5], shpj[5];
  double ai, aj, ro0i, ro0j, invrei, invrej;
  double rhoa0j, drhoa0j, rhoa0i, drhoa0i;
  double rhoa1j, drhoa1j, rhoa1i, drhoa1i;
  double rhoa2j, drhoa2j, rhoa2i, drhoa2i;
  double a3, a3a, rhoa3j, drhoa3j, rhoa3i, drhoa3i;
  double a4, a4a, rhoa4j, drhoa4j, rhoa4i, drhoa4i;
  double a5, a5a, a5aa, rhoa5j, drhoa5j, rhoa5i, drhoa5i;
  double drho0dr1, drho0dr2, drho0ds1, drho0ds2;
  double drho1dr1, drho1dr2, drho1ds1, drho1ds2;
  double drho1drm1[3], drho1drm2[3];
  double drho2dr1, drho2dr2, drho2ds1, drho2ds2;
  double drho2drm1[3], drho2drm2[3];
  double drho3dr1, drho3dr2, drho3ds1, drho3ds2;
  double drho3drm1[3], drho3drm2[3];
  double drho4dr1, drho4dr2, drho4ds1, drho4ds2;
  double drho4drm1[3], drho4drm2[3];
  double drho5dr1, drho5dr2, drho5ds1, drho5ds2;
  double drho5drm1[3], drho5drm2[3];
  double dt1dr1, dt1dr2, dt1ds1, dt1ds2;
  double dt2dr1, dt2dr2, dt2ds1, dt2ds2;
  double dt3dr1, dt3dr2, dt3ds1, dt3ds2;
  double dt4dr1, dt4dr2, dt4ds1, dt4ds2;
  double dt5dr1, dt5dr2, dt5ds1, dt5ds2;
  double drhodr1, drhodr2, drhods1, drhods2, drhodrm1[3], drhodrm2[3];
  double arg;
  double arg1i1, arg1j1, arg1i2, arg1j2, arg1i3, arg1j3, arg3i3, arg3j3;
  double arg1i4, arg1j4, arg1i5, arg1j5, arg2i4, arg2j4, arg2i5, arg2j5;
  double arg3i5, arg3j5;
  double dsij1, dsij2, force1, force2;
  double t1i, t2i, t3i, t4i, t5i, t1j, t2j, t3j, t4j, t5j;

  third = 1.0/3.0;
  sixth = 1.0/6.0;

  //  Compute forces atom i

  elti = fmap[type[i]];
  if (elti < 0) return;

  xitmp = x[i][0];
  yitmp = x[i][1];
  zitmp = x[i][2];

  //  Treat each pair
  for (jn = 0; jn < numneigh; jn++) {
    j = firstneigh[jn];
    eltj = fmap[type[j]];
    if (!iszero(scrfcn[fnoffset+jn]) && eltj >= 0) {
      sij = scrfcn[fnoffset+jn]*fcpair[fnoffset+jn];
      delij[0] = x[j][0]-xitmp;
      delij[1] = x[j][1]-yitmp;
      delij[2] = x[j][2]-zitmp;
      rij2 = delij[0]*delij[0]+delij[1]*delij[1]+delij[2]*delij[2];
      if (rij2 < this->cutforcesq) {
        rij = sqrt(rij2);
        r = rij;
        //  Compute phi and phip
        ind = this->eltind[elti][eltj];
        pp = rij*this->rdrar;
        kk = (int)pp;
        kk = std::min(kk, this->nrar-2);
        pp = pp-kk;
        pp = std::min(pp, 1.0);
        phi = ((this->phirar3[ind][kk]*pp+this->phirar2[ind][kk])*pp+
                this->phirar1[ind][kk])*pp+this->phirar[ind][kk];
        phip = (this->phirar6[ind][kk]*pp+this->phirar5[ind][kk])*pp+
                this->phirar4[ind][kk];
        recip = 1.0/r;

        if (eflag_either != 0) {
          if (eflag_global != 0)
            *eng_vdwl = *eng_vdwl+phi*sij;
          if (eflag_atom != 0) {
            eatom[i] = eatom[i]+0.5*phi*sij;
            eatom[j] = eatom[j]+0.5*phi*sij;
          }
        }

        //     write(1,*) "force_xmeamf: phi: ",phi
        //     write(1,*) "force_xmeamf: phip: ",phip

        //     Compute pair densities and derivatives
        invrei = 1.0/this->re_xmeam[elti][elti];
        ai = rij*invrei-1.0;
        ro0i = this->rho0_xmeam[elti];
        rhoa0i = ro0i*MathSpecial::fm_exp(-this->beta0_xmeam[elti]*ai);
        drhoa0i = -this->beta0_xmeam[elti]*invrei*rhoa0i;
        rhoa1i = ro0i*MathSpecial::fm_exp(-this->beta1_xmeam[elti]*ai);
        drhoa1i = -this->beta1_xmeam[elti]*invrei*rhoa1i;
        rhoa2i = ro0i*MathSpecial::fm_exp(-this->beta2_xmeam[elti]*ai);
        drhoa2i = -this->beta2_xmeam[elti]*invrei*rhoa2i;
        rhoa3i = ro0i*MathSpecial::fm_exp(-this->beta3_xmeam[elti]*ai);
        drhoa3i = -this->beta3_xmeam[elti]*invrei*rhoa3i;
        rhoa4i = ro0i*MathSpecial::fm_exp(-this->beta4_xmeam[elti]*ai);
        drhoa4i = -this->beta4_xmeam[elti]*invrei*rhoa4i;
        rhoa5i = ro0i*MathSpecial::fm_exp(-this->beta5_xmeam[elti]*ai);
        drhoa5i = -this->beta5_xmeam[elti]*invrei*rhoa5i;

        if (elti != eltj) {
          invrej = 1.0/this->re_xmeam[eltj][eltj];
          aj = rij*invrej-1.0;
          ro0j = this->rho0_xmeam[eltj];
          rhoa0j = ro0j*MathSpecial::fm_exp(-this->beta0_xmeam[eltj]*aj);
          drhoa0j = -this->beta0_xmeam[eltj]*invrej*rhoa0j;
          rhoa1j = ro0j*MathSpecial::fm_exp(-this->beta1_xmeam[eltj]*aj);
          drhoa1j = -this->beta1_xmeam[eltj]*invrej*rhoa1j;
          rhoa2j = ro0j*MathSpecial::fm_exp(-this->beta2_xmeam[eltj]*aj);
          drhoa2j = -this->beta2_xmeam[eltj]*invrej*rhoa2j;
          rhoa3j = ro0j*MathSpecial::fm_exp(-this->beta3_xmeam[eltj]*aj);
          drhoa3j = -this->beta3_xmeam[eltj]*invrej*rhoa3j;
          rhoa4j = ro0j*MathSpecial::fm_exp(-this->beta4_xmeam[eltj]*aj);
          drhoa4j = -this->beta4_xmeam[eltj]*invrej*rhoa4j;
          rhoa5j = ro0j*MathSpecial::fm_exp(-this->beta5_xmeam[eltj]*aj);
          drhoa5j = -this->beta5_xmeam[eltj]*invrej*rhoa5j;
        } else {
          rhoa0j = rhoa0i;
          drhoa0j = drhoa0i;
          rhoa1j = rhoa1i;
          drhoa1j = drhoa1i;
          rhoa2j = rhoa2i;
          drhoa2j = drhoa2i;
          rhoa3j = rhoa3i;
          drhoa3j = drhoa3i;
          rhoa4j = rhoa4i;
          drhoa4j = drhoa4i;
          rhoa5j = rhoa5i;
          drhoa5j = drhoa5i;
        }

        const double t1mi = this->t1_xmeam[elti];
        const double t2mi = this->t2_xmeam[elti];
        const double t3mi = this->t3_xmeam[elti];
        const double t4mi = this->t4_xmeam[elti];
        const double t5mi = this->t5_xmeam[elti];
        const double t1mj = this->t1_xmeam[eltj];
        const double t2mj = this->t2_xmeam[eltj];
        const double t3mj = this->t3_xmeam[eltj];
        const double t4mj = this->t4_xmeam[eltj];
        const double t5mj = this->t5_xmeam[eltj];
        if (this->ialloy == 1) {
          rhoa1j  *= t1mj;
          rhoa2j  *= t2mj;
          rhoa3j  *= t3mj;
          rhoa4j  *= t4mj;
          rhoa5j  *= t5mj;
          rhoa1i  *= t1mi;
          rhoa2i  *= t2mi;
          rhoa3i  *= t3mi;
          rhoa4i  *= t4mi;
          rhoa5i  *= t5mi;
          drhoa1j *= t1mj;
          drhoa2j *= t2mj;
          drhoa3j *= t3mj;
          drhoa4j *= t4mj;
          drhoa5j *= t5mj;
          drhoa1i *= t1mi;
          drhoa2i *= t2mi;
          drhoa3i *= t3mi;
          drhoa4i *= t4mi;
          drhoa5i *= t5mi;
        }

        nv2 = nv3 = nv4 = nv5 = 0;
        arg1i1 = arg1j1 = arg1i2 = arg1j2 = arg1i3 = arg1j3 = 0.0;
        arg3i3 = arg3j3 = arg1i4 = arg1j4 = arg1i5 = arg1j5 = 0.0;
        arg2i4 = arg2j4 = arg2i5 = arg2j5 = arg3i5 = arg3j5 = 0.0;
        for (m = 0; m < 3; ++m) {
          for (n = m; n < 3; ++n) {
            for (o = n; o < 3; ++o) {
              for (p = o; p < 3; ++p) {
                for (q = p; q < 3; ++q) {
                  arg = delij[m]*delij[n]*delij[o]*
                        delij[p]*delij[q]*this->v5D[nv5];
                  arg1i5 += arho5[i][nv5]*arg;
                  arg1j5 -= arho5[j][nv5]*arg;
                  nv5 += 1;
                }
                arg = delij[m]*delij[n]*delij[o]*
                      delij[p]*this->v4D[nv4];
                arg1i4 += arho4[i][nv4]*arg;
                arg1j4 += arho4[j][nv4]*arg;
                nv4 += 1;
              }
              arg = delij[m]*delij[n]*delij[o]*this->v3D[nv3];
              arg1i3 += arho3[i][nv3]*arg;
              arg1j3 -= arho3[j][nv3]*arg;
              arg2i5 += arho5b[i][nv3]*arg;
              arg2j5 -= arho5b[j][nv3]*arg;
              nv3 += 1;
            }
            arg = delij[m]*delij[n]*this->v2D[nv2];
            arg1i2 += arho2[i][nv2]*arg;
            arg1j2 += arho2[j][nv2]*arg;
            arg2i4 += arho4b[i][nv2]*arg;
            arg2j4 += arho4b[j][nv2]*arg;
            nv2 += 1;
          }
          arg1i1 += arho1[i][m]*delij[m];
          arg1j1 -= arho1[j][m]*delij[m];
          arg3i3 += arho3b[i][m]*delij[m];
          arg3j3 -= arho3b[j][m]*delij[m];
          arg3i5 += arho5c[i][m]*delij[m];
          arg3j5 -= arho5c[j][m]*delij[m];
        }
        // compute drho(0-5)dr
        //     rho0 terms
        drho0dr1 = drhoa0j*sij;
        drho0dr2 = drhoa0i*sij;
        //     rho1 terms
        a1 = 2.0*sij/rij;
        drho1dr1 = a1*(drhoa1j-rhoa1j/rij)*arg1i1;
        drho1dr2 = a1*(drhoa1i-rhoa1i/rij)*arg1j1;
        //     rho2 terms
        a2 = 2.0*sij/rij2;
        drho2dr1 = a2*(drhoa2j-2*rhoa2j/rij)*arg1i2
          -2.0/3.0*arho2b[i]*drhoa2j*sij;
        drho2dr2 = a2*(drhoa2i-2*rhoa2i/rij)*arg1j2
          -2.0/3.0*arho2b[j]*drhoa2i*sij;
        //     rho3 terms
        rij3 = rij*rij2;
        a3 = 2.0*sij/rij3;
        a3a = 6.0/5.0*sij/rij;
        drho3dr1 = a3*(drhoa3j-3*rhoa3j/rij)*arg1i3
          -a3a*(drhoa3j-rhoa3j/rij)*arg3i3;
        drho3dr2 = a3*(drhoa3i-3*rhoa3i/rij)*arg1j3
          -a3a*(drhoa3i-rhoa3i/rij)*arg3j3;
        //     rho4 terms
        rij4 = rij2*rij2;
        a4 = 2.0*sij/rij4;
        a4a = 12.0*sij/(7.0*rij2);
        drho4dr1 = a4*(drhoa4j-4*rhoa4j/rij)*arg1i4-a4a*(drhoa4j-2*rhoa4j/rij)
          *arg2i4+6.0/35.0*arho4c[i]*drhoa4j*sij;
        drho4dr2 = a4*(drhoa4i-4*rhoa4i/rij)*arg1j4-a4a*(drhoa4i-2*rhoa4i/rij)
          *arg2j4+6.0/35.0*arho4c[j]*drhoa4i*sij;
        //     rho5 terms
        rij5 = rij2*rij2*rij;
        a5 = 2.0*sij/rij5;
        a5a = 20.0*sij/9.0/rij3;
        a5aa = 10.0*sij/21.0/rij;
        drho5dr1 = a5*(drhoa5j-5.0*rhoa5j/rij)*arg1i5-a5a*(drhoa5j-3*rhoa5j/rij)
          *arg2i5+a5aa*(drhoa5j-rhoa5j/rij)*arg3i5;
        drho5dr2 = a5*(drhoa5i-5.0*rhoa5i/rij)*arg1j5-a5a*(drhoa5i-3*rhoa5i/rij)
          *arg2j5+a5aa*(drhoa5i-rhoa5i/rij)*arg3j5;


        // compute drho(1-5)drm1\2
        a1 = 2.0*sij/rij;
        a2 = 4.0*sij/rij2;
        a3 = 6.0*sij/rij3;
        a3a = 6.0*sij/(5.0*rij);
        a4 = 8.0*sij/rij4;
        a4a = 24.0*sij/(7.0*rij2);
        a5 = 10.0*sij/rij5;
        a5a = 20.0*sij/(3.0*rij3);
        a5aa = 10.0*sij/(21.0*rij);
        for (m = 0; m < 3; ++m) {
          drho1drm1[m] = a1*rhoa1j*arho1[i][m];
          drho1drm2[m] = -a1*rhoa1i*arho1[j][m];
          drho2drm1[m] = drho2drm2[m] = 0.0;
          drho3drm1[m] = drho3drm2[m] = 0.0;
          drho4drm1[m] = drho4drm2[m] = 0.0;
          drho5drm1[m] = drho5drm2[m] = 0.0;
          nv2 = nv3 = nv4 = 0;
          drho5drm1[m] += a5aa*arho5c[i][m];
          drho5drm2[m] += a5aa*arho5c[j][m];
          for (n = 0; n < 3; ++n) {
            drho2drm1[m] += arho2[i][this->vind2D[m][n]]*delij[n];
            drho2drm2[m] -= arho2[j][this->vind2D[m][n]]*delij[n];
            drho4drm1[m] -= a4a*arho4b[i][this->vind2D[m][n]]*delij[n];
            drho4drm2[m] += a4a*arho4b[j][this->vind2D[m][n]]*delij[n];
            for (o = n; o < 3; ++o) {
              arg = delij[n]*delij[o]*this->v2D[nv2];
              drho3drm1[m] += arho3[i][this->vind3D[m][n][o]]*arg;
              drho3drm2[m] += arho3[j][this->vind3D[m][n][o]]*arg;
              drho5drm1[m] -= a5a*arho5b[i][this->vind3D[m][n][o]]*arg;
              drho5drm2[m] -= a5a*arho5b[j][this->vind3D[m][n][o]]*arg;
              ++nv2;
              for (p = o; p < 3; ++p) {
                arg = delij[n]*delij[o]*delij[p]*this->v3D[nv3];
                drho4drm1[m] += a4*arho4[i][this->vind4D[m][n][o][p]]*arg;
                drho4drm2[m] -= a4*arho4[j][this->vind4D[m][n][o][p]]*arg;
                ++nv3;
                for (q = p; q < 3; ++q) {
                  arg = delij[n]*delij[o]*delij[p]*delij[q]*this->v4D[nv4];
                  drho5drm1[m] += a5*arho5[i][this->vind5D[m][n][o][p][q]]*arg;
                  drho5drm2[m] += a5*arho5[j][this->vind5D[m][n][o][p][q]]*arg;
                  ++nv4;
                }
              }
            }
          }
          drho2drm1[m] *= a2*rhoa2j;
          drho2drm2[m] *= -a2*rhoa2i;
          drho3drm1[m] = (a3*drho3drm1[m]-a3a*arho3b[i][m])*rhoa3j;
          drho3drm2[m] = (-a3*drho3drm2[m]+a3a*arho3b[j][m])*rhoa3i;
          drho4drm1[m] *= rhoa4j;
          drho4drm2[m] *= -rhoa4i;
          drho5drm1[m] *= rhoa5j;
          drho5drm2[m] *= -rhoa5i;
        }

        //     Compute derivatives of weighting functions t wrt rij
        t1i = t_ave[i][0];
        t2i = t_ave[i][1];
        t3i = t_ave[i][2];
        t4i = t_ave[i][3];
        t5i = t_ave[i][4];
        t1j = t_ave[j][0];
        t2j = t_ave[j][1];
        t3j = t_ave[j][2];
        t4j = t_ave[j][3];
        t5j = t_ave[j][4];

        if (this->ialloy == 1) {
          a1i = fdiv_zero(drhoa0j*sij, tsq_ave[i][0]);
          a1j = fdiv_zero(drhoa0i*sij, tsq_ave[j][0]);
          a2i = fdiv_zero(drhoa0j*sij, tsq_ave[i][1]);
          a2j = fdiv_zero(drhoa0i*sij, tsq_ave[j][1]);
          a3i = fdiv_zero(drhoa0j*sij, tsq_ave[i][2]);
          a3j = fdiv_zero(drhoa0i*sij, tsq_ave[j][2]);
          a4i = fdiv_zero(drhoa0j*sij, tsq_ave[i][3]);
          a4j = fdiv_zero(drhoa0i*sij, tsq_ave[j][3]);
          a5i = fdiv_zero(drhoa0j*sij, tsq_ave[i][4]);
          a5j = fdiv_zero(drhoa0i*sij, tsq_ave[j][4]);

          dt1dr1 = a1i*(t1mj-t1i*MathSpecial::square(t1mj));
          dt1dr2 = a1j*(t1mi-t1j*MathSpecial::square(t1mi));
          dt2dr1 = a2i*(t2mj-t2i*MathSpecial::square(t2mj));
          dt2dr2 = a2j*(t2mi-t2j*MathSpecial::square(t2mi));
          dt3dr1 = a3i*(t3mj-t3i*MathSpecial::square(t3mj));
          dt3dr2 = a3j*(t3mi-t3j*MathSpecial::square(t3mi));
          dt4dr1 = a4i*(t4mj-t4i*MathSpecial::square(t4mj));
          dt4dr2 = a4j*(t4mi-t4j*MathSpecial::square(t4mi));
          dt5dr1 = a5i*(t5mj-t5i*MathSpecial::square(t5mj));
          dt5dr2 = a5j*(t5mi-t5j*MathSpecial::square(t5mi));

        } else if (this->ialloy == 2) {
          dt1dr1 = 0.0;
          dt1dr2 = 0.0;
          dt2dr1 = 0.0;
          dt2dr2 = 0.0;
          dt3dr1 = 0.0;
          dt3dr2 = 0.0;
          dt4dr1 = 0.0;
          dt4dr2 = 0.0;
          dt5dr1 = 0.0;
          dt5dr2 = 0.0;

        } else {
          ai = 0.0;
          if (!iszero(rho0[i]))
            ai = drhoa0j*sij/rho0[i];
          aj = 0.0;
          if (!iszero(rho0[j]))
            aj = drhoa0i*sij/rho0[j];

          dt1dr1 = ai*(t1mj-t1i);
          dt1dr2 = aj*(t1mi-t1j);
          dt2dr1 = ai*(t2mj-t2i);
          dt2dr2 = aj*(t2mi-t2j);
          dt3dr1 = ai*(t3mj-t3i);
          dt3dr2 = aj*(t3mi-t3j);
          dt4dr1 = ai*(t4mj-t4i);
          dt4dr2 = aj*(t4mi-t4j);
          dt5dr1 = ai*(t5mj-t5i);
          dt5dr2 = aj*(t5mi-t5j);
        }

        //     Compute derivatives of total density wrt rij, sij and rij(3)
        compute_symm_factors_nns(elti, shpi);
				compute_symm_factors_nns(eltj, shpj);
        drhodr1 = dgamma1[i]*drho0dr1+dgamma2[i]*
                  (dt1dr1*rho1[i]+t1i*drho1dr1+dt2dr1*rho2[i]+t2i*drho2dr1
                  +dt3dr1*rho3[i]+t3i*drho3dr1+dt4dr1*rho4[i]+t4i*drho4dr1
                  +dt5dr1*rho5[i]+t5i*drho5dr1)-
                  dgamma3[i]*(shpi[0]*dt1dr1+shpi[1]*dt2dr1
                             +shpi[2]*dt3dr1+shpi[3]*dt4dr1
                             +shpi[4]*dt5dr1);
        drhodr2 = dgamma1[j]*drho0dr2+dgamma2[j]*
                  (dt1dr2*rho1[j]+t1j*drho1dr2+dt2dr2*rho2[j]+t2j*drho2dr2
                  +dt3dr2*rho3[j]+t3j*drho3dr2+dt4dr2*rho4[j]+t4j*drho4dr2
                  +dt5dr2*rho5[j]+t5j*drho5dr2)-
                  dgamma3[j]*(shpj[0]*dt1dr2+shpj[1]*dt2dr2
                              +shpj[2]*dt3dr2+shpj[3]*dt4dr2
                              +shpj[4]*dt5dr2);
        for (m = 0; m < 3; m++) {
          drhodrm1[m] = 0.0;
          drhodrm2[m] = 0.0;
          drhodrm1[m] = dgamma2[i]*
                        (t1i*drho1drm1[m]+t2i*drho2drm1[m]
                        +t3i*drho3drm1[m]+t4i*drho4drm1[m]
                        +t5i*drho5drm1[m]);
          drhodrm2[m] = dgamma2[j]*
                        (t1j*drho1drm2[m]+t2j*drho2drm2[m]
                        +t3j*drho3drm2[m]+t4j*drho4drm2[m]
                        +t5j*drho5drm2[m]);
        }

        //     Compute derivatives wrt sij, but only if necessary
        if (!iszero(dscrfcn[fnoffset+jn])) {
          drho0ds1 = rhoa0j;
          drho0ds2 = rhoa0i;
          a1 = 2.0/rij;
          drho1ds1 = a1*rhoa1j*arg1i1;
          drho1ds2 = a1*rhoa1i*arg1j1;
          a2 = 2.0/rij2;
          drho2ds1 = a2*rhoa2j*arg1i2-2.0/3.0*arho2b[i]*rhoa2j;
          drho2ds2 = a2*rhoa2i*arg1j2-2.0/3.0*arho2b[j]*rhoa2i;
          a3 = 2.0/rij3;
          a3a = 6.0/(5.0*rij);
          drho3ds1 = a3*rhoa3j*arg1i3-a3a*rhoa3j*arg3i3;
          drho3ds2 = a3*rhoa3i*arg1j3-a3a*rhoa3i*arg3j3;
          a4 = 2.0/rij2/rij2;
          a4a = 12.0/7.0/rij2;
          drho4ds1 = a4*rhoa4j*arg1i4-a4a*rhoa4j*arg2i4+6.0/35.0*rhoa4j*arho4c[i];
          drho4ds2 = a4*rhoa4i*arg1j4-a4a*rhoa4i*arg2j4+6.0/35.0*rhoa4i*arho4c[j];
          a5 = 2.0/rij/rij2/rij2;
          a5a = 20.0/9.0/rij2/rij;
          a5aa = 10.0/21.0/rij;
          drho5ds1 = a5*rhoa5j*arg1i5-a5a*rhoa5j*arg2i5+a5aa*rhoa5j*arg3i5;
          drho5ds2 = a5*rhoa5i*arg1j5-a5a*rhoa5i*arg2j5+a5aa*rhoa5i*arg3j5;

          if (this->ialloy == 1) {
            a1i = fdiv_zero(rhoa0j, tsq_ave[i][0]);
            a1j = fdiv_zero(rhoa0i, tsq_ave[j][0]);
            a2i = fdiv_zero(rhoa0j, tsq_ave[i][1]);
            a2j = fdiv_zero(rhoa0i, tsq_ave[j][1]);
            a3i = fdiv_zero(rhoa0j, tsq_ave[i][2]);
            a3j = fdiv_zero(rhoa0i, tsq_ave[j][2]);
            a4i = fdiv_zero(rhoa0j, tsq_ave[i][3]);
            a4j = fdiv_zero(rhoa0i, tsq_ave[j][3]);
            a5i = fdiv_zero(rhoa0j, tsq_ave[i][4]);
            a5j = fdiv_zero(rhoa0i, tsq_ave[j][4]);

            dt1ds1 = a1i*(t1mj-t1i*MathSpecial::square(t1mj));
            dt1ds2 = a1j*(t1mi-t1j*MathSpecial::square(t1mi));
            dt2ds1 = a2i*(t2mj-t2i*MathSpecial::square(t2mj));
            dt2ds2 = a2j*(t2mi-t2j*MathSpecial::square(t2mi));
            dt3ds1 = a3i*(t3mj-t3i*MathSpecial::square(t3mj));
            dt3ds2 = a3j*(t3mi-t3j*MathSpecial::square(t3mi));
            dt4ds1 = a4i*(t4mj-t4i*MathSpecial::square(t4mj));
            dt4ds2 = a4j*(t4mi-t4j*MathSpecial::square(t4mi));
            dt5ds1 = a5i*(t5mj-t5i*MathSpecial::square(t5mj));
            dt5ds2 = a5j*(t5mi-t5j*MathSpecial::square(t5mi));


          } else if (this->ialloy == 2) {
            dt1ds1 = 0.0;
            dt1ds2 = 0.0;
            dt2ds1 = 0.0;
            dt2ds2 = 0.0;
            dt3ds1 = 0.0;
            dt3ds2 = 0.0;
            dt4ds1 = 0.0;
            dt4ds2 = 0.0;
            dt5ds1 = 0.0;
            dt5ds2 = 0.0;
          } else {
            ai = 0.0;
            if (!iszero(rho0[i]))
              ai = rhoa0j/rho0[i];
            aj = 0.0;
            if (!iszero(rho0[j]))
              aj = rhoa0i/rho0[j];

            dt1ds1 = ai*(t1mj-t1i);
            dt1ds2 = aj*(t1mi-t1j);
            dt2ds1 = ai*(t2mj-t2i);
            dt2ds2 = aj*(t2mi-t2j);
            dt3ds1 = ai*(t3mj-t3i);
            dt3ds2 = aj*(t3mi-t3j);
            dt4ds1 = ai*(t4mj-t4i);
            dt4ds2 = aj*(t4mi-t4j);
            dt5ds1 = ai*(t5mj-t5i);
            dt5ds2 = aj*(t5mi-t5j);
          }

          drhods1 = dgamma1[i]*drho0ds1+dgamma2[i]*
          (dt1ds1*rho1[i]+t1i*drho1ds1+dt2ds1*rho2[i]+t2i*drho2ds1
          +dt3ds1*rho3[i]+t3i*drho3ds1+dt4ds1*rho4[i]+t4i*drho4ds1
          +dt5ds1*rho5[i]+t5i*drho5ds1)-
          dgamma3[i]*(shpi[0]*dt1ds1+shpi[1]*dt2ds1+shpi[2]*dt3ds1
                     +shpi[3]*dt4ds1+shpi[4]*dt5ds1);

          drhods2 = dgamma1[j]*drho0ds2+dgamma2[j]*
          (dt1ds2*rho1[j]+t1j*drho1ds2+dt2ds2*rho2[j]+t2j*drho2ds2
          +dt3ds2*rho3[j]+t3j*drho3ds2+dt4ds2*rho4[j]+t4j*drho4ds2
          +dt5ds2*rho5[j]+t5j*drho5ds2)-
          dgamma3[j]*(shpj[0]*dt1ds2+shpj[1]*dt2ds2+shpj[2]*dt3ds2
                     +shpj[3]*dt4ds2+shpj[4]*dt5ds2);
        }

        //     Compute derivatives of energy wrt rij, sij and rij[3]
        dUdrij = phip*sij+frhop[i]*drhodr1+frhop[j]*drhodr2;
        dUdsij = 0.0;
        if (!iszero(dscrfcn[fnoffset+jn])) {
          dUdsij = phi+frhop[i]*drhods1+frhop[j]*drhods2;
        }
        for (m = 0; m < 3; m++) {
          dUdrijm[m] = frhop[i]*drhodrm1[m]+frhop[j]*drhodrm2[m];
        }

        //     Add the part of the force due to dUdrij and dUdsij

        force = dUdrij*recip+dUdsij*dscrfcn[fnoffset+jn];
        for (m = 0; m < 3; m++) {
          forcem = delij[m]*force+dUdrijm[m];
          f[i][m] += forcem;
          f[j][m] -= forcem;
        }

        //     Tabulate per-atom virial as symmetrized stress tensor

        if (vflag_atom != 0) {
          fi[0] = delij[0]*force+dUdrijm[0];
          fi[1] = delij[1]*force+dUdrijm[1];
          fi[2] = delij[2]*force+dUdrijm[2];
          v[0] = -0.5*(delij[0]*fi[0]);
          v[1] = -0.5*(delij[1]*fi[1]);
          v[2] = -0.5*(delij[2]*fi[2]);
          v[3] = -0.25*(delij[0]*fi[1]+delij[1]*fi[0]);
          v[4] = -0.25*(delij[0]*fi[2]+delij[2]*fi[0]);
          v[5] = -0.25*(delij[1]*fi[2]+delij[2]*fi[1]);

          for (m = 0; m < 6; m++) {
            vatom[i][m] = vatom[i][m]+v[m];
            vatom[j][m] = vatom[j][m]+v[m];
          }
        }

        //     Now compute forces on other atoms k due to change in sij

        if (iszero(sij) || iszero(sij-1.0)) continue;  //: cont jn loop

        double dxik(0), dyik(0), dzik(0);
        double dxjk(0), dyjk(0), dzjk(0);

        for (kn = 0; kn < numneigh_full; kn++) {
          k = firstneigh_full[kn];
          eltk = fmap[type[k]];
          if (k != j && eltk >= 0) {
            double xik, xjk, cikj, sikj, dfc, a;
            double dCikj1, dCikj2;
            double delc, rik2, rjk2;

            sij = scrfcn[jn+fnoffset] * fcpair[jn+fnoffset];
            const double Cmax = this->Cmax_xmeam_rho[elti][eltj][eltk];
            const double Cmin = this->Cmin_xmeam_rho[elti][eltj][eltk];

            dsij1 = 0.0;
            dsij2 = 0.0;
            if (!iszero(sij) && !iszero(sij-1.0)) {
              const double rbound = rij2*this->ebound_xmeam[elti][eltj];
              delc = Cmax-Cmin;
              dxjk = x[k][0]-x[j][0];
              dyjk = x[k][1]-x[j][1];
              dzjk = x[k][2]-x[j][2];
              rjk2 = dxjk*dxjk+dyjk*dyjk+dzjk*dzjk;
              if (rjk2 <= rbound) {
                dxik = x[k][0]-x[i][0];
                dyik = x[k][1]-x[i][1];
                dzik = x[k][2]-x[i][2];
                rik2 = dxik*dxik+dyik*dyik+dzik*dzik;
                if (rik2 <= rbound) {
                  xik = rik2/rij2;
                  xjk = rjk2/rij2;
                  a = 1-(xik-xjk)*(xik-xjk);
                  if (!iszero(a)) {
                    cikj = (2.0*(xik+xjk)+a-2.0)/a;
                    if (cikj >= Cmin && cikj <= Cmax) {
                      cikj = (cikj-Cmin)/delc;
                      sikj = dfcut(cikj, dfc);
                      dCfunc2(rij2, rik2, rjk2, dCikj1, dCikj2);
                      a = sij/delc*dfc/sikj;
                      dsij1 = a*dCikj1;
                      dsij2 = a*dCikj2;
                    }
                  }
                }
              }
            }

            if (!iszero(dsij1) || !iszero(dsij2)) {
              force1 = dUdsij*dsij1;
              force2 = dUdsij*dsij2;

              f[i][0] += force1*dxik;
              f[i][1] += force1*dyik;
              f[i][2] += force1*dzik;
              f[j][0] += force2*dxjk;
              f[j][1] += force2*dyjk;
              f[j][2] += force2*dzjk;
              f[k][0] -= force1*dxik+force2*dxjk;
              f[k][1] -= force1*dyik+force2*dyjk;
              f[k][2] -= force1*dzik+force2*dzjk;

              //     Tabulate per-atom virial as symmetrized stress tensor

              if (vflag_atom != 0) {
                fi[0] = force1*dxik;
                fi[1] = force1*dyik;
                fi[2] = force1*dzik;
                fj[0] = force2*dxjk;
                fj[1] = force2*dyjk;
                fj[2] = force2*dzjk;
                v[0] = -third*(dxik*fi[0]+dxjk*fj[0]);
                v[1] = -third*(dyik*fi[1]+dyjk*fj[1]);
                v[2] = -third*(dzik*fi[2]+dzjk*fj[2]);
                v[3] = -sixth*(dxik*fi[1]+dxjk*fj[1]+dyik*fi[0]+dyjk*fj[0]);
                v[4] = -sixth*(dxik*fi[2]+dxjk*fj[2]+dzik*fi[0]+dzjk*fj[0]);
                v[5] = -sixth*(dyik*fi[2]+dyjk*fj[2]+dzik*fi[1]+dzjk*fj[1]);

                for (m = 0; m < 6; m++) {
                  vatom[i][m] += v[m];
                  vatom[j][m] += v[m];
                  vatom[k][m] += v[m];
                }
              }
            }
          }
          //     end of k loop
        }
      }
    }
    //     end of j loop
  }
}
