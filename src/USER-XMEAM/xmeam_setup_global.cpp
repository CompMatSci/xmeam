#include "xmeam.h"
#include <cmath>

using namespace LAMMPS_NS;

//------------------------------------------------------------------------------

template <typename TYPE, int maxi, int maxj>
static inline void setall2d(TYPE (&arr)[maxi][maxj], const TYPE v) {
  for (int i = 0; i < maxi; i++)
    for (int j = 0; j < maxj; j++)
      arr[i][j] = v;
}

template <typename TYPE, int maxi, int maxj, int maxk>
static inline void setall3d(TYPE (&arr)[maxi][maxj][maxk], const TYPE v) {
  for (int i = 0; i < maxi; i++)
    for (int j = 0; j < maxj; j++)
      for (int k = 0; k < maxk; k++)
        arr[i][j][k] = v;
}

void
XMEAM::xmeam_setup_global(int nelt, lattice_t* lat, double* z, int* ielement,
                          double* /*atwt*/, double* alpha, double* b0,
                          double* b1, double* b2, double* b3, double* b4,
                          double* b5, double* alat, double* esub, double* asub,
                          double* t0, double* t1, double* t2, double* t3,
                          double* t4, double* t5, double* rozero, int* ibar) {
  int i;
  double tmplat[maxelt];

  this->neltypes = nelt;
  for (i = 0; i < nelt; i++) {
    this->lattce_xmeam[i][i] = lat[i];

    this->Z_xmeam[i] = z[i];
    this->ielt_xmeam[i] = ielement[i];
    this->alpha_xmeam[i][i] = alpha[i];
    this->beta0_xmeam[i] = b0[i];
    this->beta1_xmeam[i] = b1[i];
    this->beta2_xmeam[i] = b2[i];
    this->beta3_xmeam[i] = b3[i];
    this->beta4_xmeam[i] = b4[i];
    this->beta5_xmeam[i] = b5[i];
    tmplat[i] = alat[i];
    this->Ec_xmeam[i][i] = esub[i];
    this->A_xmeam[i] = asub[i];
    this->t0_xmeam[i] = t0[i];
    this->t1_xmeam[i] = t1[i];
    this->t2_xmeam[i] = t2[i];
    this->t3_xmeam[i] = t3[i];
    this->t4_xmeam[i] = t4[i];
    this->t5_xmeam[i] = t5[i];
    this->rho0_xmeam[i] = rozero[i];
    this->ibar_xmeam[i] = ibar[i];

    if (this->lattce_xmeam[i][i] == FCC)
      this->re_xmeam[i][i] = tmplat[i]/sqrt(2.0);
    else if (this->lattce_xmeam[i][i] == BCC)
      this->re_xmeam[i][i] = tmplat[i]*sqrt(3.0)/2.0;
    else if (this->lattce_xmeam[i][i] == HCP)
      this->re_xmeam[i][i] = tmplat[i];
    else if (this->lattce_xmeam[i][i] == DIM)
      this->re_xmeam[i][i] = tmplat[i];
    else if (this->lattce_xmeam[i][i] == DIA)
      this->re_xmeam[i][i] = tmplat[i]*sqrt(3.0)/4.0;
    else {
      //           error
    }
  }

  // Set some defaults
  this->rc_xmeam = 4.0;
  this->delr_xmeam = 0.1;
  setall2d(this->attrac_xmeam, 0.0);
  setall2d(this->repuls_xmeam, 0.0);
  setall3d(this->Cmax_xmeam_rho, 2.8);
  setall3d(this->Cmin_xmeam_rho, 2.0);
  setall3d(this->Cmax_xmeam_pair, 2.8);
  setall3d(this->Cmin_xmeam_pair, 2.0);
  setall2d(this->ebound_xmeam, (2.8*2.8)/(4.0*(2.8-1.0)));
  setall2d(this->delta_xmeam, 0.0);
  setall2d(this->nn2_xmeam, 0);
  setall2d(this->nn4_xmeam, 0);
  setall2d(this->zbl_xmeam, 1);
  this->gsmooth_factor = 99.0;
  this->augt1 = 1;
  this->ialloy = 0;
  this->mix_ref_t = 0;
  this->emb_lin_neg = 0;
  this->bkgd_dyn = 0;
  this->erose_form = 0;
}
