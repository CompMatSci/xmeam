/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
   ------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------
   Contributing author: Sebastian Hütter (OvGU)
   Additional changes on XMEAM made by Rui Wang (CityU), 2022
 ------------------------------------------------------------------------- */

#include "xmeam.h"
#include <cstddef>
#include "memory.h"

using namespace LAMMPS_NS;

//------------------------------------------------------------------------------
XMEAM::XMEAM(Memory* mem)
  : memory(mem) {
  phir = phirar = phirar1 = phirar2
       = phirar3 = phirar4 = phirar5 = phirar6 = NULL;
  nmax = 0;
  rho = rho0 = rho1 = rho2 = rho3 = rho4 = rho5 = frhop = NULL;
  gamma = dgamma1 = dgamma2 = dgamma3 = arho2b = arho4c = NULL;
  arho1 = arho2 = arho3 = arho3b = t_ave = tsq_ave = NULL;
  arho4 = arho4b = arho5 = arho5b = arho5c = NULL;
  maxneigh = 0;
  scrfcn = dscrfcn = fcpair = NULL;

  neltypes = 0;
  for (int i = 0; i < maxelt; i++) {
    Omega_xmeam[i] = Z_xmeam[i] = A_xmeam[i] = B_xmeam[i] = rho0_xmeam[i]
                   = beta0_xmeam[i] = beta1_xmeam[i] = beta2_xmeam[i]
                   = beta3_xmeam[i] = beta4_xmeam[i] = beta5_xmeam[i]
                   = t0_xmeam[i] = t1_xmeam[i] = t2_xmeam[i]
                   = t3_xmeam[i] = t4_xmeam[i] = t5_xmeam[i]
                   = rho_ref_xmeam[i] = ibar_xmeam[i] = ielt_xmeam[i] = 0.0;
    for (int j = 0; j < maxelt; j++) {
      lattce_xmeam[i][j] = FCC;
      Ec_xmeam[i][j] = re_xmeam[i][j] = alpha_xmeam[i][j] = delta_xmeam[i][j]
      = ebound_xmeam[i][j] = attrac_xmeam[i][j] = repuls_xmeam[i][j] = 0.0;
      nn2_xmeam[i][j] = zbl_xmeam[i][j] = eltind[i][j] = 0;
    }
  }
}

XMEAM::~XMEAM() {
  memory->destroy(this->phirar6);
  memory->destroy(this->phirar5);
  memory->destroy(this->phirar4);
  memory->destroy(this->phirar3);
  memory->destroy(this->phirar2);
  memory->destroy(this->phirar1);
  memory->destroy(this->phirar);
  memory->destroy(this->phir);

  memory->destroy(this->rho);
  memory->destroy(this->rho0);
  memory->destroy(this->rho1);
  memory->destroy(this->rho2);
  memory->destroy(this->rho3);
  memory->destroy(this->rho4);
  memory->destroy(this->rho5);
  memory->destroy(this->frhop);
  memory->destroy(this->gamma);
  memory->destroy(this->dgamma1);
  memory->destroy(this->dgamma2);
  memory->destroy(this->dgamma3);
  memory->destroy(this->arho2b);

  memory->destroy(this->arho1);
  memory->destroy(this->arho2);
  memory->destroy(this->arho3);
  memory->destroy(this->arho3b);
  memory->destroy(this->t_ave);
  memory->destroy(this->tsq_ave);

  memory->destroy(this->arho4c);
  memory->destroy(this->arho4);
  memory->destroy(this->arho4b);
  memory->destroy(this->arho5);
  memory->destroy(this->arho5b);
  memory->destroy(this->arho5c);

  memory->destroy(this->scrfcn);
  memory->destroy(this->dscrfcn);
  memory->destroy(this->fcpair);
}
