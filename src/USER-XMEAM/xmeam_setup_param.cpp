#include "xmeam.h"
#include <algorithm>

using namespace LAMMPS_NS;

//------------------------------------------------------------------------------
//     do a sanity check on index parameters
void
XMEAM::xmeam_checkindex(int num, int lim, int nidx, int* idx /*idx(3)*/,
                        int* ierr) {
  //: idx[0..2]
  *ierr = 0;
  if (nidx < num) {
    *ierr = 2;
    return;
  }

  for (int i = 0; i < num; i++) {
    if ((idx[i] < 0) || (idx[i] >= lim)) {
      *ierr = 3;
      return;
    }
  }
}

//     The "which" argument corresponds to the index of the "keyword" array
//     in pair_xmeam.cpp:
//
//     0 = Ec_xmeam
//     1 = alpha_xmeam
//     2 = rho0_xmeam
//     3 = delta_xmeam
//     4 = lattce_xmeam
//     5 = attrac_xmeam
//     6 = repuls_xmeam
//     7 = nn2_xmeam
//     8 = Cmin_xmeam_rho
//     9 = Cmax_xmeam_rho
//     10 = rc_xmeam
//     11 = delr_xmeam
//     12 = augt1
//     13 = gsmooth_factor
//     14 = re_xmeam
//     15 = ialloy
//     16 = mixture_ref_t
//     17 = erose_form
//     18 = zbl_xmeam
//     19 = emb_lin_neg
//     20 = bkgd_dyn
//     21 = nn4_xmeam
//     22 = bsub_xmeam
//     23 = Cmin_xmeam_pair
//     24 = Cmin_xmeam_pair

void
XMEAM::xmeam_setup_param(int which, double value, int nindex,
                         int* index /*index(3)*/, int* errorflag) {
  //: index[0..2]
  int i1, i2;
  lattice_t vlat;
  *errorflag = 0;

  switch (which) {
    //     0 = Ec_xmeam
    case 0:
      xmeam_checkindex(2, neltypes, nindex, index, errorflag);
      if (*errorflag != 0)
        return;
      this->Ec_xmeam[index[0]][index[1]] = value;
      break;

    //     1 = alpha_xmeam
    case 1:
      xmeam_checkindex(2, neltypes, nindex, index, errorflag);
      if (*errorflag != 0)
        return;
      this->alpha_xmeam[index[0]][index[1]] = value;
      break;

    //     2 = rho0_xmeam
    case 2:
      xmeam_checkindex(1, neltypes, nindex, index, errorflag);
      if (*errorflag != 0)
        return;
      this->rho0_xmeam[index[0]] = value;
      break;

    //     3 = delta_xmeam
    case 3:
      xmeam_checkindex(2, neltypes, nindex, index, errorflag);
      if (*errorflag != 0)
        return;
      this->delta_xmeam[index[0]][index[1]] = value;
      break;

    //     4 = lattce_xmeam
    case 4:
      xmeam_checkindex(2, neltypes, nindex, index, errorflag);
      if (*errorflag != 0)
        return;
      vlat = (lattice_t)value;

      this->lattce_xmeam[index[0]][index[1]] = vlat;
      break;

    //     5 = attrac_xmeam
    case 5:
      xmeam_checkindex(2, neltypes, nindex, index, errorflag);
      if (*errorflag != 0)
        return;
      this->attrac_xmeam[index[0]][index[1]] = value;
      break;

    //     6 = repuls_xmeam
    case 6:
      xmeam_checkindex(2, neltypes, nindex, index, errorflag);
      if (*errorflag != 0)
        return;
      this->repuls_xmeam[index[0]][index[1]] = value;
      break;

    //     7 = nn2_xmeam
    case 7:
      xmeam_checkindex(2, neltypes, nindex, index, errorflag);
      if (*errorflag != 0)
        return;
      i1 = std::min(index[0], index[1]);
      i2 = std::max(index[0], index[1]);
      this->nn2_xmeam[i1][i2] = (int)value;
      break;

    //     8 = Cmin_xmeam_rho
    case 8:
      xmeam_checkindex(3, neltypes, nindex, index, errorflag);
      if (*errorflag != 0)
        return;
      this->Cmin_xmeam_rho[index[0]][index[1]][index[2]] = value;
      break;

    //     9 = Cmax_xmeam_rho
    case 9:
      xmeam_checkindex(3, neltypes, nindex, index, errorflag);
      if (*errorflag != 0)
        return;
      this->Cmax_xmeam_rho[index[0]][index[1]][index[2]] = value;
      break;

    //     10 = rc_xmeam
    case 10:
      this->rc_xmeam = value;
      break;

    //     11 = delr_xmeam
    case 11:
      this->delr_xmeam = value;
      break;

    //     12 = augt1
    case 12:
      this->augt1 = (int)value;
      break;

    //     13 = gsmooth
    case 13:
      this->gsmooth_factor = value;
      break;

    //     14 = re_xmeam
    case 14:
      xmeam_checkindex(2, neltypes, nindex, index, errorflag);
      if (*errorflag != 0)
        return;
      this->re_xmeam[index[0]][index[1]] = value;
      break;

    //     15 = ialloy
    case 15:
      this->ialloy = (int)value;
      break;

    //     16 = mixture_ref_t
    case 16:
      this->mix_ref_t = (int)value;
      break;

    //     17 = erose_form
    case 17:
      this->erose_form = (int)value;
      break;

    //     18 = zbl_xmeam
    case 18:
      xmeam_checkindex(2, neltypes, nindex, index, errorflag);
      if (*errorflag != 0)
        return;
      i1 = std::min(index[0], index[1]);
      i2 = std::max(index[0], index[1]);
      this->zbl_xmeam[i1][i2] = (int)value;
      break;

    //     19 = emb_lin_neg
    case 19:
      this->emb_lin_neg = (int)value;
      break;

    //     20 = bkgd_dyn
    case 20:
      this->bkgd_dyn = (int)value;
      break;
		//     21 = nn4
	  case 21:
			xmeam_checkindex(2, neltypes, nindex, index, errorflag);
      if (*errorflag != 0)
        return;
      i1 = std::min(index[0], index[1]);
      i2 = std::max(index[0], index[1]);
      this->nn4_xmeam[i1][i2] = (int)value;
      break;
    //     22 = bsub 
    case 22:
      xmeam_checkindex(1, neltypes, nindex, index, errorflag);
      if (*errorflag != 0)
        return;
      this->B_xmeam[index[0]] = (double)value;
      break;
    //     23 = Cmin_xmeam_pair 
    case 23:
      xmeam_checkindex(3, neltypes, nindex, index, errorflag);
      if (*errorflag != 0)
        return;
      this->Cmin_xmeam_pair[index[0]][index[1]][index[2]] = value;
      break;
    //     24 = Cmax_xmeam_pair
    case 24:
      xmeam_checkindex(3, neltypes, nindex, index, errorflag);
      if (*errorflag != 0)
        return;
      this->Cmax_xmeam_pair[index[0]][index[1]][index[2]] = value;
      break;
    default:
      *errorflag = 1;
  }
}
