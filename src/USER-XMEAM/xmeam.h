#ifndef LMP_XMEAM_H
#define LMP_XMEAM_H

#include <cmath>

#define maxelt 5

namespace LAMMPS_NS {
class Memory;

typedef enum {FCC, BCC, HCP, DIM, DIA, B1, C11, L12, B2} lattice_t;

class XMEAM {
public:
  XMEAM(Memory* mem);
  ~XMEAM();

private:
  Memory* memory;
  // cutforce = force cutoff
  // cutforcesq = force cutoff squared
  // Ec_xmeam = cohesive energy
  // re_xmeam = nearest-neighbor distance
  // Omega_xmeam = atomic volume
  // B_xmeam = bulk modulus
  // Z_xmeam = number of first neighbors for reference structure
  // ielt_xmeam = atomic number of element
  // A_xmeam = adjustable parameter
  // alpha_xmeam = sqrt(9*Omega*B/Ec)
  // rho0_xmeam = density scaling parameter
  // delta_xmeam = heat of formation for alloys
  // beta[0-5]_xmeam = electron density constants
  // t[0-5]_xmeam = coefficients on densities in Gamma computation
  // rho_ref_xmeam = background density for reference structure
  // ibar_xmeam(i) = selection parameter for Gamma function for elt i,
  // lattce_xmeam(i,j) = lattce configuration for elt i or alloy (i,j)
  // neltypes = maximum number of element type defined
  // eltind = index number of pair (similar to Voigt notation; ij = ji)
  // phir = pair potential function array
  // phirar[1-6] = spline coeffs
  // attrac_xmeam = attraction parameter in Rose energy
  // repuls_xmeam = repulsion parameter in Rose energy
  // nn2_xmeam = 1 if second nearest neighbors are to be computed, else 0
  // nn4_xmeam = 1 if forth nearest neighbors are to be computed, else 0
  // zbl_xmeam = 1 if zbl potential for small r to be use, else 0
  // emb_lin_neg = 1 if linear embedding function for rhob to be used, else 0
  // bkgd_dyn = 1 if reference densities follows Dynamo, else 0
  // Cmin_xmeam_rho, Cmax_meam_rho = min and max values in rho screening cutoff
  // Cmin_xmeam_pair, Cmax_meam_pair = min and max values in pair screening cutoff
  // rc_xmeam = cutoff distance for meam
  // delr_xmeam = cutoff region for meam
  // ebound_xmeam = factor giving maximum boundary of sceen fcn ellipse
  // augt1 = flag for whether t1 coefficient should be augmented
  // ialloy = flag for newer alloy formulation (as in dynamo code)
  // mix_ref_t = flag to recover "old" way of computing t in reference config
  // erose_form = selection parameter for form of E_rose function
  // gsmooth_factor = factor determining length of G smoothing region
  // vind[2345]D = Voight notation index maps for 2,3,4,5D
  // v2D,v3D,v4D,v5D = array of factors to apply for Voight notation
  // nr,dr = pair function discretization parameters
  // nrar,rdrar = spline coeff array parameters

  double cutforce, cutforcesq;
  double Ec_xmeam[maxelt][maxelt], re_xmeam[maxelt][maxelt];
  double Omega_xmeam[maxelt], Z_xmeam[maxelt];
  double A_xmeam[maxelt], alpha_xmeam[maxelt][maxelt], rho0_xmeam[maxelt];
  double delta_xmeam[maxelt][maxelt];
  double beta0_xmeam[maxelt], beta1_xmeam[maxelt], beta2_xmeam[maxelt];
  double beta3_xmeam[maxelt], beta4_xmeam[maxelt], beta5_xmeam[maxelt];
  double t0_xmeam[maxelt], t1_xmeam[maxelt], t2_xmeam[maxelt];
  double t3_xmeam[maxelt], t4_xmeam[maxelt], t5_xmeam[maxelt];
  double rho_ref_xmeam[maxelt];
  int ibar_xmeam[maxelt], ielt_xmeam[maxelt];
  lattice_t lattce_xmeam[maxelt][maxelt];
  int nn2_xmeam[maxelt][maxelt];
	int nn4_xmeam[maxelt][maxelt];
  double B_xmeam[maxelt];
  int zbl_xmeam[maxelt][maxelt];
  int eltind[maxelt][maxelt];
  int neltypes;

  double** phir;

  double **phirar, **phirar1, **phirar2, **phirar3;
  double **phirar4, **phirar5, **phirar6;

  double attrac_xmeam[maxelt][maxelt], repuls_xmeam[maxelt][maxelt];

  double Cmin_xmeam_rho[maxelt][maxelt][maxelt];
  double Cmax_xmeam_rho[maxelt][maxelt][maxelt];
  double Cmin_xmeam_pair[maxelt][maxelt][maxelt];
  double Cmax_xmeam_pair[maxelt][maxelt][maxelt];
  
  double rc_xmeam, delr_xmeam, ebound_xmeam[maxelt][maxelt];
  int augt1, ialloy, mix_ref_t, erose_form;
  int emb_lin_neg, bkgd_dyn;
  double gsmooth_factor;

  int vind2D[3][3], vind3D[3][3][3], vind4D[3][3][3][3], vind5D[3][3][3][3][3];
  // x-y-z to Voigt-like index
  int v2D[6], v3D[10], v4D[15], v5D[21];
  // multiplicity of Voigt index (i.e. [1] -> xy+yx = 2

  int nr, nrar;
  double dr, rdrar;

public:
  int nmax;
  double *rho, *rho0, *rho1, *rho2, *rho3, *rho4, *rho5, *frhop;
  double *gamma, *dgamma1, *dgamma2, *dgamma3, *arho2b;
  double **arho1, **arho2, **arho3, **arho3b, **t_ave, **tsq_ave;
  double *arho4c, **arho4, **arho4b, **arho5, **arho5b, **arho5c;
  int maxneigh;
  double *scrfcn, *dscrfcn, *fcpair;

protected:
  // xmeam_funcs.cpp

  //---------------------------------------------------------------------------------
  // Cutoff function
  //
  static double fcut(const double xi) {
    double a;
    if (xi >= 1.0)
      return 1.0;
    else if (xi <= 0.0)
      return 0.0;
    else {
      // [1-(1-xi)^4]^2
      a = 1.0-xi;
      a *= a;
      a *= a;
      a = 1.0-a;
      return a*a;
    }
  }

  //---------------------------------------------------------------------------------
  // Cutoff function and derivative
  //
  static double dfcut(const double xi, double& dfc) {
    double a, a3, a4, a1m4;
    if (xi >= 1.0) {
      dfc = 0.0;
      return 1.0;
    } else if (xi <= 0.0) {
      dfc = 0.0;
      return 0.0;
    } else {
      a = 1.0-xi;
      a3 = a*a*a;
      a4 = a*a3;
      a1m4 = 1.0-a4;
      dfc = 8*a1m4*a3;
      return a1m4*a1m4;
    }
  }
  //---------------------------------------------------------------------------------
  // Derivative of Cikj w.r.t. rij
  //     Inputs: rij, rij2, rik2, rjk2
  //
  static double dCfunc(const double rij2, const double rik2,
                       const double rjk2) {
    double rij4, a, b, bsq, denom;
    rij4 = rij2*rij2;
    a = rik2+rjk2;
    b = rik2-rjk2;
    bsq = b*b;
    denom = rij4-bsq;
    denom *= denom;
    return -4*(rij4*a+a*bsq-2*rij2*bsq)/denom;
  }

  //----------------------------------------------------------------------------
  // Derivative of Cikj w.r.t. rik and rjk2
  //     Inputs: rij2, rik2, rjk2
  //
  static void dCfunc2(const double rij2, const double rik2, const double rjk2,
                      double& dCikj1, double& dCikj2) {
    double rij4, rik4, rjk4, a, denom;
    rij4 = rij2*rij2;
    rik4 = rik2*rik2;
    rjk4 = rjk2*rjk2;
    a = rik2-rjk2;
    denom = rij4-a*a;
    denom *= denom;
    dCikj1 = 4*rij2*(rij4+rik4+2*rik2*rjk2-3*rjk4-2*rij2*a)/denom;
    dCikj2 = 4*rij2*(rij4+rjk4+2*rik2*rjk2-3*rik4+2*rij2*a)/denom;
  }
  double G_gam(const double gamma, const int ibar, int &errorflag) const;
  double dG_gam(const double gamma, const int ibar, double &dG) const;
  static double zbl(const double r, const int z1, const int z2);
  double embedding(const double A, const double B, const double Ec, const double rhobar,
                   double& dF) const;
  static double erose(const double r, const double re, const double alpha,
                      const double Ec, const double repuls,
                      const double attrac, const int form);

  static void get_shpfcn(const lattice_t latt, double (&s)[5]);
  static int get_Zij(const lattice_t latt);
  static int get_Zij2(const lattice_t latt, const double cmin,
                      const double cmax, const double re,
                      const double rc, const double delr,
                      double &a, double &S);
  static int get_Zij3(const lattice_t latt, const double cmin,
                      const double cmax, const double re,
                      const double rc, const double delr,
                      double &a, double &S);
  static int get_Zij4(const lattice_t latt, const double cmin,
                      const double cmax, const double re,
                      const double rc, const double delr,
                      double &a, double &S);




protected:
  void xmeam_checkindex(int, int, int, int*, int*);
  void xmeam_voigt_sort(int*, int);
  void getscreen(int i, double* scrfcn, double* dscrfcn, double* fcpair,
                 double** x, int numneigh, int* firstneigh, int numneigh_full,
                 int* firstneigh_full, int ntype, int* type, int* fmap);
  void calc_rho1(int i, int ntype, int* type, int* fmap, double** x,
                 int numneigh, int* firstneigh, double* scrfcn,
                 double* fcpair);

  void alloyparams();
  void compute_pair_xmeam();
  double phi_xmeam(double, int, int);
  void compute_reference_density();
  void get_tavref(double*, double*, double*, double*, double*, double*,
                  double*, double*, double*, double*, double, double,
                  double, double, double, double, double, double,
                  double, double, double, int, int, lattice_t);
  void get_sijk_rho(double, int, int, int, double*);
  void get_sijk_pair(double, int, int, int, double*);
  void get_densref(double, int, int, double*, double*, double*, double*,
                   double*, double*, double*, double*, double*, double*,
                   double*, double*);
  void compute_symm_factors_nns(int, double (&s)[5]);
	void interpolate_xmeam(int);

public:
  void xmeam_setup_global(int nelt, lattice_t* lat, double* z, int* ielement,
                          double* atwt, double* alpha, double* b0, double* b1,
                          double* b2, double* b3, double* b4, double* b5,
                          double* alat, double* esub, double* asub, double* t0,
                          double* t1, double* t2, double* t3, double* t4,
                          double* t5, double* rozero, int* ibar);
  void xmeam_setup_param(int which, double value, int nindex, int* index
                         /*index(3)*/, int* errorflag);
  void xmeam_setup_done(double* cutmax);
  void xmeam_dens_setup(int atom_nmax, int nall, int n_neigh);
  void xmeam_dens_init(int i, int ntype, int* type, int* fmap, double** x,
                       int numneigh, int* firstneigh, int numneigh_full,
                       int* firstneigh_full, int fnoffset);
  void xmeam_dens_final(int nlocal, int eflag_either, int eflag_global,
                        int eflag_atom, double* eng_vdwl, double* eatom,
                        int ntype, int* type, int* fmap, int& errorflag);
  void xmeam_force(int i, int eflag_either, int eflag_global, int eflag_atom,
                   int vflag_atom, double* eng_vdwl, double* eatom, int ntype,
                   int* type, int* fmap, double** x, int numneigh,
                   int* firstneigh, int numneigh_full, int* firstneigh_full,
                   int fnoffset, double** f, double** vatom);
};

// Functions we need for compat

static inline bool iszero(const double f) {
  return fabs(f) < 1e-20;
}

// Helper functions

static inline double fdiv_zero(const double n, const double d) {
  if (iszero(d))
    return 0.0;
  return n / d;
}

static inline int factorial(const int n) {
	if (iszero(n))
		return 1;
	return n*factorial(n-1);
}

}
#endif
