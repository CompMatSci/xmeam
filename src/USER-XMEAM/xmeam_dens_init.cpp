#include "xmeam.h"
#include <cmath>
#include "memory.h"
#include "math_special.h"

using namespace LAMMPS_NS;

void
XMEAM::xmeam_dens_setup(int atom_nmax, int nall, int n_neigh) {
  int i, j;

  // grow local arrays if necessary

  if (atom_nmax > nmax) {
    memory->destroy(rho);
    memory->destroy(rho0);
    memory->destroy(rho1);
    memory->destroy(rho2);
    memory->destroy(rho3);
    memory->destroy(rho4);
    memory->destroy(rho5);
    memory->destroy(frhop);
    memory->destroy(gamma);
    memory->destroy(dgamma1);
    memory->destroy(dgamma2);
    memory->destroy(dgamma3);
    memory->destroy(arho2b);
    memory->destroy(arho1);
    memory->destroy(arho2);
    memory->destroy(arho3);
    memory->destroy(arho3b);
    memory->destroy(t_ave);
    memory->destroy(tsq_ave);
    memory->destroy(arho4c);
    memory->destroy(arho4);
    memory->destroy(arho4b);
    memory->destroy(arho5);
    memory->destroy(arho5b);
    memory->destroy(arho5c);
    nmax = atom_nmax;

    memory->create(rho, nmax, "pair:rho");
    memory->create(rho0, nmax, "pair:rho0");
    memory->create(rho1, nmax, "pair:rho1");
    memory->create(rho2, nmax, "pair:rho2");
    memory->create(rho3, nmax, "pair:rho3");
    memory->create(rho4, nmax, "pair:rho4");
    memory->create(rho5, nmax, "pair:rho5");
    memory->create(frhop, nmax, "pair:frhop");
    memory->create(gamma, nmax, "pair:gamma");
    memory->create(dgamma1, nmax, "pair:dgamma1");
    memory->create(dgamma2, nmax, "pair:dgamma2");
    memory->create(dgamma3, nmax, "pair:dgamma3");
    memory->create(arho2b, nmax, "pair:arho2b");
    memory->create(arho1, nmax, 3, "pair:arho1");
    memory->create(arho2, nmax, 6, "pair:arho2");
    memory->create(arho3, nmax, 10, "pair:arho3");
    memory->create(arho3b, nmax, 3, "pair:arho3b");
    memory->create(t_ave, nmax, 5, "pair:t_ave");
    memory->create(tsq_ave, nmax, 5, "pair:tsq_ave");
    memory->create(arho4c, nmax, "pair:arho4c");
    memory->create(arho4, nmax, 15, "pair:arho4");
    memory->create(arho4b, nmax, 6, "pair:arho4b");
    memory->create(arho5, nmax, 21, "pair:arho5");
    memory->create(arho5b, nmax, 10, "pair:arho5b");
    memory->create(arho5c, nmax, 3, "pair:arho5c");
  }

  if (n_neigh > maxneigh) {
    memory->destroy(scrfcn);
    memory->destroy(dscrfcn);
    memory->destroy(fcpair);
    maxneigh = n_neigh;
    memory->create(scrfcn, maxneigh, "pair:scrfcn");
    memory->create(dscrfcn, maxneigh, "pair:dscrfcn");
    memory->create(fcpair, maxneigh, "pair:fcpair");
  }

  // zero out local arrays

  for (i = 0; i < nall; ++i) {
    rho0[i] = 0.0;
    arho2b[i] = 0.0;
    arho4c[i] = 0.0;
    for (j = 0; j < 3; ++j) {
      arho1[i][j] = arho2[i][j] = arho3[i][j] = arho4[i][j] = arho5[i][j] = 0.0;
      arho3b[i][j] = arho4b[i][j] = arho5b[i][j] = arho5c[i][j] = 0.0;
      t_ave[i][j] = tsq_ave[i][j] = 0.0;
    }
    for (j = 3; j < 5; ++j) {
      arho2[i][j] = arho3[i][j] = arho4[i][j] = arho5[i][j] = 0.0;
      arho4b[i][j] = arho5b[i][j] = t_ave[i][j] = tsq_ave[i][j] = 0.0;
    }
    for (j = 5; j < 6; ++j) {
      arho2[i][j] = arho3[i][j] = arho4[i][j] = arho5[i][j] = 0.0;
      arho4b[i][j] = arho5b[i][j] = 0.0;
    }
    for (j = 6; j < 10; ++j) {
      arho3[i][j] = arho4[i][j] = arho5[i][j] = arho5b[i][j] = 0.0;
    }
    for (j = 10; j < 15; ++j) {
      arho4[i][j] = arho5[i][j] = 0.0;
    }
    for (j = 15; j < 21; ++j) {
      arho5[i][j] = 0.0;
    }
  }
}

void
XMEAM::xmeam_dens_init(int i, int ntype, int* type, int* fmap, double** x,
                     int numneigh, int* firstneigh,
                     int numneigh_full, int* firstneigh_full, int fnoffset) {
  //     Compute screening function and derivatives
  getscreen(i, &scrfcn[fnoffset], &dscrfcn[fnoffset], &fcpair[fnoffset],
            x, numneigh, firstneigh, numneigh_full, firstneigh_full,
            ntype, type, fmap);

  //     Calculate intermediate density terms to be communicated
  calc_rho1(i, ntype, type, fmap, x, numneigh, firstneigh,
            &scrfcn[fnoffset], &fcpair[fnoffset]);
}

//------------------------------------------------------------------------------

void
XMEAM::getscreen(int i, double* scrfcn, double* dscrfcn, double* fcpair,
                 double** x, int numneigh, int* firstneigh,
                 int numneigh_full, int* firstneigh_full, int ,
                 int* type, int* fmap) {
  int jn, j, kn, k;
  int elti, eltj, eltk;
  double xitmp, yitmp, zitmp, delxij, delyij, delzij, rij2, rij;
  double xjtmp, yjtmp, zjtmp, delxik, delyik, delzik, rik2 /*,rik*/;
  double xktmp, yktmp, zktmp, delxjk, delyjk, delzjk, rjk2 /*,rjk*/;
  double xik, xjk, sij, fcij, sfcij, dfcij, sikj, dfikj, cikj;
  double Cmin, Cmax, delc, /*ebound,*/ a, coef1, coef2;
  double dCikj;
  double rnorm, fc, dfc, drinv;

  drinv = 1.0/this->delr_xmeam;
  elti = fmap[type[i]];
  if (elti < 0) return;

  xitmp = x[i][0];
  yitmp = x[i][1];
  zitmp = x[i][2];

  for (jn = 0; jn < numneigh; ++jn) {
    j = firstneigh[jn];

    eltj = fmap[type[j]];
    if (eltj < 0) continue;

    //     First compute screening function itself, sij
    xjtmp = x[j][0];
    yjtmp = x[j][1];
    zjtmp = x[j][2];
    delxij = xjtmp-xitmp;
    delyij = yjtmp-yitmp;
    delzij = zjtmp-zitmp;
    rij2 = delxij*delxij+delyij*delyij+delzij*delzij;

    if (rij2 > this->cutforcesq) {
      dscrfcn[jn] = 0.0;
      scrfcn[jn] = 0.0;
      fcpair[jn] = 0.0;
      continue;
    }

    const double rbound = this->ebound_xmeam[elti][eltj]*rij2;
    rij = sqrt(rij2);
    rnorm = (this->cutforce-rij)*drinv;
    sij = 1.0;

    //     if rjk2 > ebound*rijsq, atom k is definitely outside the ellipse
    for (kn = 0; kn < numneigh_full; ++kn) {
      k = firstneigh_full[kn];
      if (k == j) continue;
      eltk = fmap[type[k]];
      if (eltk < 0) continue;

      xktmp = x[k][0];
      yktmp = x[k][1];
      zktmp = x[k][2];

      delxjk = xktmp-xjtmp;
      delyjk = yktmp-yjtmp;
      delzjk = zktmp-zjtmp;
      rjk2 = delxjk*delxjk+delyjk*delyjk+delzjk*delzjk;
      if (rjk2 > rbound) continue;

      delxik = xktmp-xitmp;
      delyik = yktmp-yitmp;
      delzik = zktmp-zitmp;
      rik2 = delxik*delxik+delyik*delyik+delzik*delzik;
      if (rik2 > rbound) continue;

      xik = rik2/rij2;
      xjk = rjk2/rij2;
      a = 1-(xik-xjk)*(xik-xjk);
      //     if a < 0, then ellipse equation doesn't describe this case and
      //     atom k can't possibly screen i-j
      if (a <= 0.0) continue;

      cikj = (2.0*(xik+xjk)+a-2.0)/a;
      Cmax = this->Cmax_xmeam_rho[elti][eltj][eltk];
      Cmin = this->Cmin_xmeam_rho[elti][eltj][eltk];
      if (cikj >= Cmax) continue;
      //     note that cikj may be slightly negative (within numerical
      //     tolerance) if atoms are colinear, so don't reject that case here
      //     (other negative cikj cases were handled by the test on "a" above)
      else if (cikj <= Cmin) {
        sij = 0.0;
        break;
      } else {
        delc = Cmax-Cmin;
        cikj = (cikj-Cmin)/delc;
        sikj = fcut(cikj);
      }
      sij *= sikj;
    }

    fc = dfcut(rnorm, dfc);
    fcij = fc;
    dfcij = dfc*drinv;

    //     Now compute derivatives
    dscrfcn[jn] = 0.0;
    sfcij = sij*fcij;
    if (!iszero(sfcij) && !iszero(sfcij-1.0)) {
      for (kn = 0; kn < numneigh_full; ++kn) {
        k = firstneigh_full[kn];
        if (k == j) continue;
        eltk = fmap[type[k]];
        if (eltk < 0) continue;

        delxjk = x[k][0]-xjtmp;
        delyjk = x[k][1]-yjtmp;
        delzjk = x[k][2]-zjtmp;
        rjk2 = delxjk*delxjk+delyjk*delyjk+delzjk*delzjk;
        if (rjk2 > rbound) continue;

        delxik = x[k][0]-xitmp;
        delyik = x[k][1]-yitmp;
        delzik = x[k][2]-zitmp;
        rik2 = delxik*delxik+delyik*delyik+delzik*delzik;
        if (rik2 > rbound) continue;

        xik = rik2/rij2;
        xjk = rjk2/rij2;
        a = 1-(xik-xjk)*(xik-xjk);
        //     if a < 0, then ellipse equation doesn't describe this case and
        //     atom k can't possibly screen i-j
        if (a <= 0.0) continue;

        cikj = (2.0*(xik+xjk)+a-2.0)/a;
        Cmax = this->Cmax_xmeam_rho[elti][eltj][eltk];
        Cmin = this->Cmin_xmeam_rho[elti][eltj][eltk];
        if (cikj >= Cmax) {
          continue;
          //     Note that cikj may be slightly negative (within numerical
          //     tolerance) if atoms are colinear, so don't reject that case
          //     here
          //     (other negative cikj cases were handled by the test on "a"
          //     above)
          //     Note that we never have 0<cikj<Cmin here, else sij=0
          //     (rejected above)
        } else {
          delc = Cmax-Cmin;  
          cikj = (cikj-Cmin)/delc;
          sikj = dfcut(cikj, dfikj);
          coef1 = dfikj/(delc*sikj);
          dCikj = dCfunc(rij2, rik2, rjk2);
          dscrfcn[jn] = dscrfcn[jn]+coef1*dCikj;
        }
      }
      coef1 = sfcij;
      coef2 = sij*dfcij/rij;
      dscrfcn[jn] = dscrfcn[jn]*coef1-coef2;
    }

    scrfcn[jn] = sij;
    fcpair[jn] = fcij;
  }
}

//------------------------------------------------------------------------------

void
XMEAM::calc_rho1(int i, int /*ntype*/, int* type, int* fmap, double** x,
                 int numneigh, int* firstneigh, double* scrfcn,
                 double* fcpair) {
  int jn, j, m, n, p, q, r, elti, eltj, nv2, nv3, nv4, nv5;
  double xtmp, ytmp, ztmp, delij[3], rij2, rij, sij, ro0i, ro0j;
  double ai, aj, rhoa0i, rhoa0j, rhoa1i, rhoa1j, rhoa2i, rhoa2j;
  double rhoa3i, rhoa3j, rhoa4i, rhoa4j, rhoa5i, rhoa5j;
  double A1i, A2i, A3i, A4i, A5i, A1j, A2j, A3j, A4j, A5j;
  double B3i, B3j, B4i, B4j, B5i, B5j, C5i, C5j;
  elti = fmap[type[i]];
  xtmp = x[i][0];
  ytmp = x[i][1];
  ztmp = x[i][2];
  for (jn = 0; jn < numneigh; ++jn) {
    if (!iszero(scrfcn[jn])) {
      j = firstneigh[jn];
      sij = scrfcn[jn]*fcpair[jn];
      delij[0] = x[j][0]-xtmp;
      delij[1] = x[j][1]-ytmp;
      delij[2] = x[j][2]-ztmp;
      rij2 = delij[0]*delij[0]+delij[1]*delij[1]+delij[2]*delij[2];
      if (rij2 < this->cutforcesq) {
        eltj = fmap[type[j]];
        rij = sqrt(rij2);
        ai = rij/this->re_xmeam[elti][elti]-1.0;
        aj = rij/this->re_xmeam[eltj][eltj]-1.0;
        ro0i = this->rho0_xmeam[elti];
        ro0j = this->rho0_xmeam[eltj];
        rhoa0j = ro0j*MathSpecial::fm_exp(-this->beta0_xmeam[eltj]*aj)*sij;
        rhoa1j = ro0j*MathSpecial::fm_exp(-this->beta1_xmeam[eltj]*aj)*sij;
        rhoa2j = ro0j*MathSpecial::fm_exp(-this->beta2_xmeam[eltj]*aj)*sij;
        rhoa3j = ro0j*MathSpecial::fm_exp(-this->beta3_xmeam[eltj]*aj)*sij;
        rhoa4j = ro0j*MathSpecial::fm_exp(-this->beta4_xmeam[eltj]*aj)*sij;
        rhoa5j = ro0j*MathSpecial::fm_exp(-this->beta5_xmeam[eltj]*aj)*sij;
        rhoa0i = ro0i*MathSpecial::fm_exp(-this->beta0_xmeam[elti]*ai)*sij;
        rhoa1i = ro0i*MathSpecial::fm_exp(-this->beta1_xmeam[elti]*ai)*sij;
        rhoa2i = ro0i*MathSpecial::fm_exp(-this->beta2_xmeam[elti]*ai)*sij;
        rhoa3i = ro0i*MathSpecial::fm_exp(-this->beta3_xmeam[elti]*ai)*sij;
        rhoa4i = ro0i*MathSpecial::fm_exp(-this->beta4_xmeam[elti]*ai)*sij;
        rhoa5i = ro0i*MathSpecial::fm_exp(-this->beta5_xmeam[elti]*ai)*sij;

        if (this->ialloy == 1) {
          rhoa1j *= this->t1_xmeam[eltj];
          rhoa2j *= this->t2_xmeam[eltj];
          rhoa3j *= this->t3_xmeam[eltj];
          rhoa4j *= this->t4_xmeam[eltj];
          rhoa5j *= this->t5_xmeam[eltj];
          rhoa1i *= this->t1_xmeam[elti];
          rhoa2i *= this->t2_xmeam[elti];
          rhoa3i *= this->t3_xmeam[elti];
          rhoa4i *= this->t4_xmeam[elti];
          rhoa5i *= this->t5_xmeam[elti];
        }

        // For ialloy = 2, use single-element value (not average)
        if (this->ialloy != 2) {
          t_ave[i][0] += this->t1_xmeam[eltj]*rhoa0j;
          t_ave[i][1] += this->t2_xmeam[eltj]*rhoa0j;
          t_ave[i][2] += this->t3_xmeam[eltj]*rhoa0j;
          t_ave[i][3] += this->t4_xmeam[eltj]*rhoa0j;
          t_ave[i][4] += this->t5_xmeam[eltj]*rhoa0j;
          t_ave[j][0] += this->t1_xmeam[elti]*rhoa0i;
          t_ave[j][1] += this->t2_xmeam[elti]*rhoa0i;
          t_ave[j][2] += this->t3_xmeam[elti]*rhoa0i;
          t_ave[j][3] += this->t4_xmeam[elti]*rhoa0i;
          t_ave[j][4] += this->t5_xmeam[elti]*rhoa0i;
        }
        if (this->ialloy == 1) {
          tsq_ave[i][0] += this->t1_xmeam[eltj]*this->t1_xmeam[eltj]*rhoa0j;
          tsq_ave[i][1] += this->t2_xmeam[eltj]*this->t2_xmeam[eltj]*rhoa0j;
          tsq_ave[i][2] += this->t3_xmeam[eltj]*this->t3_xmeam[eltj]*rhoa0j;
          tsq_ave[i][3] += this->t4_xmeam[eltj]*this->t4_xmeam[eltj]*rhoa0j;
          tsq_ave[i][4] += this->t5_xmeam[eltj]*this->t5_xmeam[eltj]*rhoa0j;
          tsq_ave[j][0] += this->t1_xmeam[elti]*this->t1_xmeam[elti]*rhoa0i;
          tsq_ave[j][1] += this->t2_xmeam[elti]*this->t2_xmeam[elti]*rhoa0i;
          tsq_ave[j][2] += this->t3_xmeam[elti]*this->t3_xmeam[elti]*rhoa0i;
          tsq_ave[j][3] += this->t4_xmeam[elti]*this->t4_xmeam[elti]*rhoa0i;
          tsq_ave[j][4] += this->t5_xmeam[elti]*this->t5_xmeam[elti]*rhoa0i;
        }
        rho0[i] += rhoa0j;
        rho0[j] += rhoa0i;
        arho2b[i] += rhoa2j;
        arho2b[j] += rhoa2i;
        arho4c[i] += rhoa4j;
        arho4c[j] += rhoa4i;

        A1j = rhoa1j/rij;
        A2j = rhoa2j/rij2;
        A3j = rhoa3j/(rij2*rij);
        A4j = rhoa4j/(rij2*rij2);
        A5j = rhoa5j/(rij2*rij2*rij);
        B3j = rhoa3j/rij;
        B4j = rhoa4j/rij2;
        B5j = rhoa5j/(rij2*rij);
        C5j = rhoa5j/rij;
        A1i = rhoa1i/rij;
        A2i = rhoa2i/rij2;
        A3i = rhoa3i/(rij2*rij);
        A4i = rhoa4i/(rij2*rij2);
        A5i = rhoa5i/(rij2*rij2*rij);
        B3i = rhoa3i/rij;
        B4i = rhoa4i/rij2;
        B5i = rhoa5i/(rij2*rij);
        C5i = rhoa5i/rij;
        nv2 = nv3 = nv4 = nv5 = 0;
        for (m = 0; m < 3; ++m) {
          arho1[i][m] += A1j*delij[m];
          arho1[j][m] -= A1i*delij[m];
          arho3b[i][m] += B3j*delij[m];
          arho3b[j][m] -= B3i*delij[m];
          arho5c[i][m] += C5j*delij[m];
          arho5c[j][m] -= C5i*delij[m];
          for (n = m; n < 3; ++n) {
            arho2[i][nv2] += A2j*delij[m]*delij[n];
            arho2[j][nv2] += A2i*delij[m]*delij[n];
            arho4b[i][nv2] += B4j*delij[m]*delij[n];
            arho4b[j][nv2] += B4i*delij[m]*delij[n];
            nv2 += 1;
            for (p = n; p < 3; ++p) {
              arho3[i][nv3] += A3j*delij[m]*delij[n]*delij[p];
              arho3[j][nv3] -= A3i*delij[m]*delij[n]*delij[p];
              arho5b[i][nv3] += B5j*delij[m]*delij[n]*delij[p];
              arho5b[j][nv3] -= B5i*delij[m]*delij[n]*delij[p];
              nv3 += 1;
              for (q = p; q < 3; ++q) {
                arho4[i][nv4] += A4j*delij[m]*delij[n]*delij[p]*delij[q];
                arho4[j][nv4] += A4i*delij[m]*delij[n]*delij[p]*delij[q];
                nv4 += 1;
                for (r = q; r < 3; ++r) {
                  arho5[i][nv5] += A5j*delij[m]*delij[n]*delij[p]
                                      *delij[q]*delij[r];
                  arho5[j][nv5] -= A5i*delij[m]*delij[n]*delij[p]
                                      *delij[q]*delij[r];
                  nv5 += 1;
                }
              }
            }
          }
        }
      }
    }
  }
}
