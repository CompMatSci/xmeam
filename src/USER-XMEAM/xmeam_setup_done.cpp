#include "xmeam.h"
#include <cmath>
#include <cstddef>
#include <algorithm>
#include "math_special.h"
#include "memory.h"

using namespace LAMMPS_NS;

//------------------------------------------------------------------------------

void
XMEAM::xmeam_setup_done(double* cutmax)
{
  int nv2, nv3, nv4, nv5, m, n, p, q, r;
  //     Force cutoff
  this->cutforce = this->rc_xmeam;
  this->cutforcesq = this->cutforce * this->cutforce;

  //     Pass cutoff back to calling program
  *cutmax = this->cutforce;

  //     Augment t1 term
  for (int i = 0; i < maxelt; i++)
    this->t1_xmeam[i] = this->t1_xmeam[i]+this->augt1*3.0/5.0*this->t3_xmeam[i];

  //     Compute off-diagonal alloy parameters
  alloyparams();

  // indices and factors for Voight notation
  nv2 = nv3 = nv4 = nv5 = 0;
  for (m = 0; m < 3; m++) {
    for (n = 0; n < 3; n++) {
      if (m <= n) {
        vind2D[m][n] = nv2;
        nv2++;
      }
      else {
        int arr[] = {m, n};
        std::sort(arr, arr+2);
        vind2D[m][n] = vind2D[arr[0]][arr[1]];
      }
      for (p = 0; p < 3; p++) {
        if (m <= n && n <= p) {
          vind3D[m][n][p] = nv3;
          nv3++;
        }
        else {
          int arr[] = {m, n, p};
          std::sort(arr, arr+3);
          vind3D[m][n][p] = vind3D[arr[0]][arr[1]][arr[2]];
        }
        for (q = 0; q < 3; q++) {
          if (m <= n && n <= p && p <= q) {
            vind4D[m][n][p][q] = nv4;
            nv4++;
          }
          else {
            int arr[] = {m, n, p, q};
            std::sort(arr, arr+4);
            vind4D[m][n][p][q] = vind4D[arr[0]][arr[1]]
            [arr[2]][arr[3]];
          }
            for (r = 0; r < 3; r++) {
              if (m <= n && n <= p && p <= q && q <= r) {
                vind5D[m][n][p][q][r] = nv5;
                nv5++;
              }
              else {
                int arr[] = {m, n, p, q, r};
                std::sort(arr, arr+5);
                vind5D[m][n][p][q][r] = vind5D[arr[0]][arr[1]][arr[2]]
                  [arr[3]][arr[4]];
              }
          }
        }
      }
    }
  }


  this->v2D[0] = 1;
  this->v2D[1] = 2;
  this->v2D[2] = 2;
  this->v2D[3] = 1;
  this->v2D[4] = 2;
  this->v2D[5] = 1;

  this->v3D[0] = 1;
  this->v3D[1] = 3;
  this->v3D[2] = 3;
  this->v3D[3] = 3;
  this->v3D[4] = 6;
  this->v3D[5] = 3;
  this->v3D[6] = 1;
  this->v3D[7] = 3;
  this->v3D[8] = 3;
  this->v3D[9] = 1;

  this->v4D[0] = 1;
  this->v4D[1] = 4;
  this->v4D[2] = 4;
  this->v4D[3] = 6;
  this->v4D[4] = 12;
  this->v4D[5] = 6;
  this->v4D[6] = 4;
  this->v4D[7] = 12;
  this->v4D[8] = 12;
  this->v4D[9] = 4;
  this->v4D[10] = 1;
  this->v4D[11] = 4;
  this->v4D[12] = 6;
  this->v4D[13] = 4;
  this->v4D[14] = 1;

  this->v5D[0] = 1;
  this->v5D[1] = 5;
  this->v5D[2] = 5;
  this->v5D[3] = 10;
  this->v5D[4] = 20;
  this->v5D[5] = 10;
  this->v5D[6] = 10;
  this->v5D[7] = 30;
  this->v5D[8] = 30;
  this->v5D[9] = 10;
  this->v5D[10] = 5;
  this->v5D[11] = 20;
  this->v5D[12] = 30;
  this->v5D[13] = 20;
  this->v5D[14] = 5;
  this->v5D[15] = 1;
  this->v5D[16] = 5;
  this->v5D[17] = 10;
  this->v5D[18] = 10;
  this->v5D[19] = 5;
  this->v5D[20] = 1;



  nv2 = 0;
  for (m = 0; m < this->neltypes; m++) {
    for (n = m; n < this->neltypes; n++) {
      this->eltind[m][n] = nv2;
      this->eltind[n][m] = nv2;
      nv2 += 1;
    }
  }

  //     Compute background densities for reference structure
  compute_reference_density();

  //     Compute pair potentials and setup arrays for interpolation
  this->nr = 1000;
  this->dr = 1.1*this->rc_xmeam/this->nr;
  compute_pair_xmeam();
}

//------------------------------------------------------------------------------
// Fill off-diagonal alloy parameters
void
XMEAM::alloyparams(void)
{

  int i, j, k;
  double eb;

  // Loop over pairs
  for (i = 0; i < this->neltypes; i++) {
    for (j = 0; j < this->neltypes; j++) {
      // Treat off-diagonal pairs
      // If i>j, set all equal to i<j case (which has aready been set,
      // here or in the input file)
      if (i > j) {
        this->re_xmeam[i][j] = this->re_xmeam[j][i];
        this->Ec_xmeam[i][j] = this->Ec_xmeam[j][i];
        this->alpha_xmeam[i][j] = this->alpha_xmeam[j][i];
        this->lattce_xmeam[i][j] = this->lattce_xmeam[j][i];
        this->nn2_xmeam[i][j] = this->nn2_xmeam[j][i];
        // If i<j and term is unset, use default values (e.g. mean of i-i and
        // j-j)
      } else if (j > i) {
        if (iszero(this->Ec_xmeam[i][j])) {
          if (this->lattce_xmeam[i][j] == L12)
            this->Ec_xmeam[i][j] = (3*this->Ec_xmeam[i][i]+this->Ec_xmeam[j][j])
                                   /4.0-this->delta_xmeam[i][j];
          else if (this->lattce_xmeam[i][j] == C11) {
            if (this->lattce_xmeam[i][i] == DIA)
              this->Ec_xmeam[i][j] = (2*this->Ec_xmeam[i][i]+this->Ec_xmeam[j][j])
                                     /3.0-this->delta_xmeam[i][j];
            else
              this->Ec_xmeam[i][j] = (this->Ec_xmeam[i][i]+2*this->Ec_xmeam[j][j])
                                     /3.0-this->delta_xmeam[i][j];
          } else
            this->Ec_xmeam[i][j] = (this->Ec_xmeam[i][i]+this->Ec_xmeam[j][j])
                                   /2.0-this->delta_xmeam[i][j];
        }
        if (iszero(this->alpha_xmeam[i][j]))
          this->alpha_xmeam[i][j] = (this->alpha_xmeam[i][i]+this->alpha_xmeam[j][j])
                                    /2.0;
        if (iszero(this->re_xmeam[i][j]))
          this->re_xmeam[i][j] = (this->re_xmeam[i][i]+this->re_xmeam[j][j])/2.0;
      }
    }
  }

  // Cmin[i][k][j] is symmetric in i-j, but not k.  For all triplets
  // where i>j, set equal to the i<j element.  Likewise for Cmax.
  for (i = 1; i < this->neltypes; i++) {
    for (j = 0; j < i; j++) {
      for (k = 0; k < this->neltypes; k++) {
        this->Cmin_xmeam_rho[i][j][k] = this->Cmin_xmeam_rho[j][i][k];
        this->Cmax_xmeam_rho[i][j][k] = this->Cmax_xmeam_rho[j][i][k];
        this->Cmin_xmeam_pair[i][j][k] = this->Cmin_xmeam_pair[j][i][k];
        this->Cmax_xmeam_pair[i][j][k] = this->Cmax_xmeam_pair[j][i][k];
      }
    }
  }

  // ebound gives the squared distance such that, for rik2 or rjk2>ebound,
  // atom k definitely lies outside the screening function ellipse (so
  // there is no need to calculate its effects).  Here, compute it for all
  // triplets [i][j][k] so that ebound[i][j] is the maximized over k
  for (i = 0; i < this->neltypes; i++) {
    for (j = 0; j < this->neltypes; j++) {
      for (k = 0; k < this->neltypes; k++) {
        eb = (this->Cmax_xmeam_rho[i][j][k]*this->Cmax_xmeam_rho[i][j][k])/
             (4.0*(this->Cmax_xmeam_rho[i][j][k]-1.0));
        this->ebound_xmeam[i][j] = std::max(this->ebound_xmeam[i][j], eb);
        eb = (this->Cmax_xmeam_pair[i][j][k]*this->Cmax_xmeam_pair[i][j][k])/
          (4.0*(this->Cmax_xmeam_pair[i][j][k]-1.0));
        this->ebound_xmeam[i][j] = std::max(this->ebound_xmeam[i][j], eb);
      }
    }
  }
}

//-----------------------------------------------------------------------
// compute XMEAM pair potential for each pair of element types
//

void
XMEAM::compute_pair_xmeam(void)
{

  double r /*ununsed:, temp*/;
  int j, a, b, nv2;
  double astar, frac, phizbl;
  int n, nmax, Z1, Z2;
	int ni, nj, nk;
	int Z3, Z4;
  double arat, rarat, scrn;
  double arat2, arat3, arat4;
  double scrn2, scrn3, scrn4;
	double Cnijk;
  double phiaa, phibb /*unused:,phitmp*/;
  double C, s111, s112, s221, S11, S22;

  // check for previously allocated arrays and free them
  if (this->phir != NULL)
    memory->destroy(this->phir);
  if (this->phirar != NULL)
    memory->destroy(this->phirar);
  if (this->phirar1 != NULL)
    memory->destroy(this->phirar1);
  if (this->phirar2 != NULL)
    memory->destroy(this->phirar2);
  if (this->phirar3 != NULL)
    memory->destroy(this->phirar3);
  if (this->phirar4 != NULL)
    memory->destroy(this->phirar4);
  if (this->phirar5 != NULL)
    memory->destroy(this->phirar5);
  if (this->phirar6 != NULL)
    memory->destroy(this->phirar6);

  // allocate memory for array that defines the potential
  memory->create(this->phir, (this->neltypes*(this->neltypes+1))/2,
                 this->nr, "pair:phir");

  // allocate coeff memory

  memory->create(this->phirar, (this->neltypes*(this->neltypes+1))/2,
                 this->nr, "pair:phirar");
  memory->create(this->phirar1, (this->neltypes*(this->neltypes+1))/2,
                 this->nr, "pair:phirar1");
  memory->create(this->phirar2, (this->neltypes*(this->neltypes+1))/2,
                 this->nr, "pair:phirar2");
  memory->create(this->phirar3, (this->neltypes*(this->neltypes+1))/2,
                 this->nr, "pair:phirar3");
  memory->create(this->phirar4, (this->neltypes*(this->neltypes+1))/2,
                 this->nr, "pair:phirar4");
  memory->create(this->phirar5, (this->neltypes*(this->neltypes+1))/2,
                 this->nr, "pair:phirar5");
  memory->create(this->phirar6, (this->neltypes*(this->neltypes+1))/2,
                 this->nr, "pair:phirar6");

  // loop over pairs of element types
  nv2 = 0;
  for (a = 0; a < this->neltypes; a++) {
    for (b = a; b < this->neltypes; b++) {
      // loop over r values and compute
      for (j = 0; j < this->nr; j++) {
        r = j*this->dr;

        this->phir[nv2][j] = phi_xmeam(r, a, b);

        // if using second-nearest neighbor, solve recursive problem
        // (see Lee and Baskes, PRB 62(13):8564 eqn.(21))
        if (this->nn2_xmeam[a][b] == 1) {
          Z1 = get_Zij(this->lattce_xmeam[a][b]);
          Z2 = get_Zij2(this->lattce_xmeam[a][b], this->Cmin_xmeam_pair[a][a][b],
												this->Cmax_xmeam_pair[a][a][b], this->re_xmeam[a][b],
                        this->rc_xmeam, this->delr_xmeam, arat, scrn);

          //     The B1, B2,  and L12 cases with NN2 have a trick to them; we need to
          //     compute the contributions from second nearest neighbors, like a-a
          //     pairs, but need to include NN2 contributions to those pairs as
          //     well.
          if (this->lattce_xmeam[a][b] == B1 || this->lattce_xmeam[a][b] == B2 ||
              this->lattce_xmeam[a][b] == L12 || this->lattce_xmeam[a][b] == DIA) {
            rarat = r*arat;

            //               phi_aa
            phiaa = phi_xmeam(rarat, a, a);
            Z1 = get_Zij(this->lattce_xmeam[a][a]);
            Z2 = get_Zij2(this->lattce_xmeam[a][a], this->Cmin_xmeam_pair[a][a][a],
													this->Cmax_xmeam_pair[a][a][a], this->re_xmeam[a][a],
                          this->rc_xmeam, this->delr_xmeam, arat, scrn);
            nmax = 10;
            if (scrn > 0.0) {
              for (n = 1; n <= nmax; n++) {
                phiaa = phiaa+pow((-Z2*scrn/Z1), n)*
                        phi_xmeam(rarat*pow(arat, n), a, a);
              }
            }

            //               phi_bb
            phibb = phi_xmeam(rarat, b, b);
            Z1 = get_Zij(this->lattce_xmeam[b][b]);
            Z2 = get_Zij2(this->lattce_xmeam[b][b], this->Cmin_xmeam_pair[b][b][b],
													this->Cmax_xmeam_pair[b][b][b], this->re_xmeam[b][b],
                          this->rc_xmeam, this->delr_xmeam, arat, scrn);
            nmax = 10;
            if (scrn > 0.0) {
              for (n = 1; n <= nmax; n++) {
                phibb = phibb+pow((-Z2*scrn/Z1), n)*
                        phi_xmeam(rarat*pow(arat, n), b, b);
              }
            }

            if (this->lattce_xmeam[a][b] == B1 || this->lattce_xmeam[a][b] == B2 ||
                this->lattce_xmeam[a][b] == DIA) {
              //     Add contributions to the B1 or B2 potential
              Z1 = get_Zij(this->lattce_xmeam[a][b]);
              Z2 = get_Zij2(this->lattce_xmeam[a][b], this->Cmin_xmeam_pair[a][a][b],
														this->Cmax_xmeam_pair[a][a][b], this->re_xmeam[a][b],
                            this->rc_xmeam, this->delr_xmeam, arat, scrn);
              this->phir[nv2][j] = this->phir[nv2][j]-Z2*scrn/(2*Z1)*phiaa;
              Z2 = get_Zij2(this->lattce_xmeam[a][b], this->Cmin_xmeam_pair[b][b][a],
														this->Cmax_xmeam_pair[b][b][a], this->re_xmeam[b][a],
                            this->rc_xmeam, this->delr_xmeam, arat, scrn2);
              this->phir[nv2][j] = this->phir[nv2][j]-Z2*scrn2/(2*Z1)*phibb;

            } else if (this->lattce_xmeam[a][b] == L12) {
              //     The L12 case has one last trick; we have to be careful to
              //     compute
              //     the correct screening between 2nd-neighbor pairs.  1-1
              //     second-neighbor pairs are screened by 2 type 1 atoms and
              //     two type
              //     2 atoms.  2-2 second-neighbor pairs are screened by 4 type
              //     1
              //     atoms.
              C = 1.0;
              get_sijk_pair(C, a, a, a, &s111);
              get_sijk_pair(C, a, a, b, &s112);
              get_sijk_pair(C, b, b, a, &s221);
              S11 = s111*s111*s112*s112;
              S22 = pow(s221, 4);
              this->phir[nv2][j] = this->phir[nv2][j]-0.75*S11*phiaa-0.25*S22*phibb;
            }

          }
          else {
            if (this->nn4_xmeam[a][b] == 1) {
              nmax = 10;
              Z2 = get_Zij2(this->lattce_xmeam[a][b], this->Cmin_xmeam_pair[a][a][b],
                            this->Cmax_xmeam_pair[a][a][b], this->re_xmeam[a][b],
                            this->rc_xmeam, this->delr_xmeam, arat2, scrn2);
              Z3 = get_Zij3(this->lattce_xmeam[a][b], this->Cmin_xmeam_pair[a][a][b],
                            this->Cmax_xmeam_pair[a][a][b], this->re_xmeam[a][b],
                            this->rc_xmeam, this->delr_xmeam, arat3, scrn3);
              Z4 = get_Zij4(this->lattce_xmeam[a][b], this->Cmin_xmeam_pair[a][a][b],
                            this->Cmax_xmeam_pair[a][a][b], this->re_xmeam[a][b],
                            this->rc_xmeam, this->delr_xmeam, arat4, scrn4);

              for (n = 1; n <= nmax; n++) {
                for (ni = 0; ni <= n; ni++) {
                  for (nj = 0; nj <= n-ni; nj++) {
                    nk = n-ni-nj;
										Cnijk = factorial(ni)*factorial(nj)*factorial(nk);
										Cnijk = factorial(n)/Cnijk;
										this->phir[nv2][j] = this->phir[nv2][j] + pow(-1,n)*Cnijk*
											pow((Z2*scrn2/Z1),ni)*pow((Z3*scrn3/Z1),nj)*pow((Z4*scrn4/Z1),nk)
											*phi_xmeam(r*pow(arat2,ni)*pow(arat3,nj)*pow(arat4,nk), a, b);
                  }
                }
              }
            }
            else {
              nmax = 10;
              for (n = 1; n <= nmax; n++) {
                this->phir[nv2][j] = this->phir[nv2][j]+pow((-Z2*scrn/Z1), n)
                                   *phi_xmeam(r*pow(arat, n), a, b);
              }
            }
          }
        }

        // For Zbl potential:
        // if astar <= -3
        //   potential is zbl potential
        // else if -3 < astar < -1
        //   potential is linear combination with zbl potential
        // endif
        if (this->zbl_xmeam[a][b] == 1) {
          astar = this->alpha_xmeam[a][b]*(r/this->re_xmeam[a][b]-1.0);
          if (astar <= -3.0)
            this->phir[nv2][j] = zbl(r, this->ielt_xmeam[a], this->ielt_xmeam[b]);
          else if (astar > -3.0 && astar < -1.0) {
            frac = fcut(1-(astar+1.0)/(-3.0+1.0));
            phizbl = zbl(r, this->ielt_xmeam[a], this->ielt_xmeam[b]);
            this->phir[nv2][j] = frac*this->phir[nv2][j]+(1-frac)*phizbl;
          }
        }
      }

      // call interpolation
      interpolate_xmeam(nv2);

      nv2 += 1;
    }
  }
}

//------------------------------------------------------------------------------
// Compute XMEAM pair potential for distance r, element types a and b
//
double
XMEAM::phi_xmeam(double r, int a, int b)
{
  /*unused:double a1,a2,a12;*/
  double t11av, t21av, t31av, t41av, t51av, t12av, t22av, t32av, t42av, t52av;
  double G1, G2, s1[5], s2[5], rho0_1, rho0_2;
  double Gam1, Gam2, Z1, Z2;
  double rhobar1, rhobar2, F1, F2, dF;
  double rho01, rho11, rho21, rho31, rho41, rho51;
  double rho02, rho12, rho22, rho32, rho42, rho52;
  double scalfac, phiaa, phibb;
  double Eu;
  double arat, scrn /*unused:,scrn2*/;
  int Z12, errorflag;
  int n, nmax, Z1nn, Z2nn;
  lattice_t latta /*unused:,lattb*/;
  double rho_bkgd1, rho_bkgd2;

  double phi_m = 0.0;

  // Equation numbers below refer to:
  //   I. Huang et.al., Modelling simul. Mater. Sci. Eng. 3:615

  // get number of neighbors in the reference structure
  //   Nref[i][j] = # of i's neighbors of type j
  Z12 = get_Zij(this->lattce_xmeam[a][b]);

  // if r is larger than rc, just return zero
  if (r >= 1.1*this->rc_xmeam)
    return 0.0;

  get_densref(r, a, b, &rho01, &rho11, &rho21, &rho31, &rho41, &rho51, &rho02,
              &rho12, &rho22, &rho32, &rho42, &rho52);

  // if densities are too small, numerical problems may result; just return zero
  if (rho01 <= 1e-14 && rho02 <= 1e-14)
    return 0.0;

  // calculate average weighting factors for the reference structure
  if (this->lattce_xmeam[a][b] == C11) {
    if (this->ialloy == 2) {
      t11av = this->t1_xmeam[a];
      t12av = this->t1_xmeam[b];
      t21av = this->t2_xmeam[a];
      t22av = this->t2_xmeam[b];
      t31av = this->t3_xmeam[a];
      t32av = this->t3_xmeam[b];
      t41av = this->t4_xmeam[a];
      t42av = this->t4_xmeam[b];
      t51av = this->t5_xmeam[a];
      t52av = this->t5_xmeam[b];
    } else {
      scalfac = 1.0/(rho01+rho02);
      t11av = scalfac*(this->t1_xmeam[a]*rho01+this->t1_xmeam[b]*rho02);
      t12av = t11av;
      t21av = scalfac*(this->t2_xmeam[a]*rho01+this->t2_xmeam[b]*rho02);
      t22av = t21av;
      t31av = scalfac*(this->t3_xmeam[a]*rho01+this->t3_xmeam[b]*rho02);
      t32av = t31av;
      t41av = scalfac*(this->t4_xmeam[a]*rho01+this->t4_xmeam[b]*rho02);
      t42av = t41av;
      t51av = scalfac*(this->t5_xmeam[a]*rho01+this->t5_xmeam[b]*rho02);
      t52av = t51av;
    }
  } else {
    // average weighting factors for the reference structure, eqn. I.8
    get_tavref(&t11av, &t21av, &t31av, &t41av, &t51av, &t12av, &t22av,
               &t32av, &t42av, &t52av, this->t1_xmeam[a], this->t2_xmeam[a],
               this->t3_xmeam[a], this->t4_xmeam[a], this->t5_xmeam[a],
               this->t1_xmeam[b], this->t2_xmeam[b], this->t3_xmeam[b],
               this->t4_xmeam[b], this->t5_xmeam[b], r, a, b,
               this->lattce_xmeam[a][b]);
  }

  // for c11b structure, calculate background electron densities
  if (this->lattce_xmeam[a][b] == C11) {
    latta = this->lattce_xmeam[a][a];
    if (latta == DIA) {
      rhobar1 = MathSpecial::square((Z12/2)*(rho02+rho01))
                +t11av*MathSpecial::square(rho12-rho11)
                +t21av/6.0*MathSpecial::square(rho22+rho21)
                +121.0/40.0*t31av*MathSpecial::square(rho32-rho31);
      rhobar1 = sqrt(rhobar1);
      rhobar2 = MathSpecial::square(Z12*rho01)
                +2.0/3.0*t21av*MathSpecial::square(rho21);
      rhobar2 = sqrt(rhobar2);
    } else {
      rhobar2 = MathSpecial::square((Z12/2)*(rho01+rho02))
                +t12av*MathSpecial::square(rho11-rho12)
                +t22av/6.0*MathSpecial::square(rho21+rho22)
                +121.0/40.0*t32av*MathSpecial::square(rho31-rho32);
      rhobar2 = sqrt(rhobar2);
      rhobar1 = MathSpecial::square(Z12*rho02)
                +2.0/3.0*t22av*MathSpecial::square(rho22);
      rhobar1 = sqrt(rhobar1);
    }
  } else {
    // for other structures, use formalism developed in Huang's paper
    //
    //     composition-dependent scaling, equation I.7
    //     If using mixing rule for t, apply to reference structure; else
    //     use precomputed values
    if (this->mix_ref_t == 1) {
      Z1 = this->Z_xmeam[a];
      Z2 = this->Z_xmeam[b];
      if (this->ibar_xmeam[a] <= 0)
        G1 = 1.0;
      else {
				compute_symm_factors_nns(a,s1);
        Gam1 = (s1[0]*t11av+s1[1]*t21av+s1[2]*t31av
               +s1[3]*t41av+s1[4]*t51av)/(Z1*Z1);
        G1 = G_gam(Gam1, this->ibar_xmeam[a], errorflag);
      }
      if (this->ibar_xmeam[b] <= 0)
        G2 = 1.0;
      else {
				compute_symm_factors_nns(b, s2);
				Gam2 = (s2[0]*t12av+s2[1]*t22av+s2[2]*t32av
               +s2[3]*t42av+s2[4]*t52av)/(Z2*Z2);
        G2 = G_gam(Gam2, this->ibar_xmeam[b], errorflag);
      }
      rho0_1 = this->rho0_xmeam[a]*Z1*G1;
      rho0_2 = this->rho0_xmeam[b]*Z2*G2;
    }
    Gam1 = (t11av*rho11+t21av*rho21+t31av*rho31+t41av*rho41+t51av*rho51);
    if (rho01 < 1.0e-14)
      Gam1 = 0.0;
    else
      Gam1 = Gam1/(rho01*rho01);

    Gam2 = (t12av*rho12+t22av*rho22+t32av*rho32+t42av*rho42+t52av*rho52);
    if (rho02 < 1.0e-14)
      Gam2 = 0.0;
    else
      Gam2 = Gam2/(rho02*rho02);

    G1 = G_gam(Gam1, this->ibar_xmeam[a], errorflag);
    G2 = G_gam(Gam2, this->ibar_xmeam[b], errorflag);
    if (this->mix_ref_t == 1) {
      rho_bkgd1 = rho0_1;
      rho_bkgd2 = rho0_2;
    } else {
      if (this->bkgd_dyn == 1) {
        rho_bkgd1 = this->rho0_xmeam[a]*this->Z_xmeam[a];
        rho_bkgd2 = this->rho0_xmeam[b]*this->Z_xmeam[b];
      } else {
        rho_bkgd1 = this->rho_ref_xmeam[a];
        rho_bkgd2 = this->rho_ref_xmeam[b];
      }
    }
    rhobar1 = rho01/rho_bkgd1*G1;
    rhobar2 = rho02/rho_bkgd2*G2;
  }

  // compute embedding functions, eqn I.5

  F1 = embedding(this->A_xmeam[a], this->B_xmeam[a], this->Ec_xmeam[a][a], rhobar1, dF);
  F2 = embedding(this->A_xmeam[b], this->B_xmeam[b], this->Ec_xmeam[b][b], rhobar2, dF);


  // compute Rose function, I.16
  Eu = erose(r, this->re_xmeam[a][b], this->alpha_xmeam[a][b],
             this->Ec_xmeam[a][b], this->repuls_xmeam[a][b],
             this->attrac_xmeam[a][b], this->erose_form);

  // calculate the pair energy
  if (this->lattce_xmeam[a][b] == C11) {
    latta = this->lattce_xmeam[a][a];
    if (latta == DIA) {
      phiaa = phi_xmeam(r, a, a);
      phi_m = (3*Eu-F2-2*F1-5*phiaa)/Z12;
    } else {
      phibb = phi_xmeam(r, b, b);
      phi_m = (3*Eu-F1-2*F2-5*phibb)/Z12;
    }
  } else if (this->lattce_xmeam[a][b] == L12) {
    phiaa = phi_xmeam(r, a, a);
    //       account for second neighbor a-a potential here...
    Z1nn = get_Zij(this->lattce_xmeam[a][a]);
    Z2nn = get_Zij2(this->lattce_xmeam[a][a], this->Cmin_xmeam_pair[a][a][a],
										this->Cmax_xmeam_pair[a][a][a], this->re_xmeam[a][a],
                    this->rc_xmeam, this->delr_xmeam, arat, scrn);
    nmax = 10;
    if (scrn > 0.0) {
      for (n = 1; n <= nmax; n++) {
        phiaa = phiaa+pow((-Z2nn*scrn/Z1nn), n)
                     *phi_xmeam(r*pow(arat, n), a, a);
      }
    }
    phi_m = Eu/3.0-F1/4.0-F2/12.0-phiaa;

  } else {
    //
    // potential is computed from Rose function and embedding energy
    phi_m = (2*Eu-F1-F2)/Z12;
    //
  }

  // if r = 0, just return 0
  if (iszero(r)) {
    phi_m = 0.0;
  }

  return phi_m;
}

//----------------------------------------------------------------------c
// Compute background density for reference structure of each element
void
XMEAM::compute_reference_density(void)
{
  int a, Z, Z2, errorflag;
  double gam, Gbar, shp[5];
  double rho0, rho0_2nn, rho0_3nn, rho0_4nn, arat, scrn;

  // loop over element types
  for (a = 0; a < this->neltypes; a++) {
    Z = (int)this->Z_xmeam[a];

    //     The zeroth order density in the reference structure, with
    //     equilibrium spacing, is just the number of first neighbors times
    //     the rho0_xmeam coefficient...
    rho0 = this->rho0_xmeam[a]*Z;

    //     ...unless we have unscreened second neighbors, in which case we
    //     add on the contribution from those (accounting for partial
    //     screening)
    if (this->nn2_xmeam[a][a] == 1) {
      Z2 = get_Zij2(this->lattce_xmeam[a][a], this->Cmin_xmeam_rho[a][a][a],
										this->Cmax_xmeam_rho[a][a][a], this->re_xmeam[a][a],
                    this->rc_xmeam, this->delr_xmeam, arat, scrn);
      rho0_2nn = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta0_xmeam[a]
                                                         *(arat-1));
      rho0 = rho0+Z2*rho0_2nn*scrn;
    }

		//     ...support 4NN for HCP, then add on the contribution from those
		//     neighbors
		if (this->nn4_xmeam[a][a] == 1) {
			int Z3,Z4;
			Z3 = get_Zij3(this->lattce_xmeam[a][a], this->Cmin_xmeam_rho[a][a][a],
										this->Cmax_xmeam_rho[a][a][a], this->re_xmeam[a][a],
                    this->rc_xmeam, this->delr_xmeam, arat, scrn);
			rho0_3nn = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta0_xmeam[a]
																												 *(arat-1));
			rho0 = rho0+Z3*rho0_3nn*scrn;
			Z4 = get_Zij4(this->lattce_xmeam[a][a], this->Cmin_xmeam_rho[a][a][a],
										this->Cmax_xmeam_rho[a][a][a], this->re_xmeam[a][a],
                    this->rc_xmeam, this->delr_xmeam, arat, scrn);
			rho0_4nn = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta0_xmeam[a]
																												 *(arat-1));
			rho0 = rho0+Z4*rho0_4nn*scrn;
		}
		if (this->ibar_xmeam[a] <= 0)
      Gbar = 1.0;
    else {
			compute_symm_factors_nns(a,shp);
      gam = (this->t1_xmeam[a]*shp[0]+this->t2_xmeam[a]*shp[1]
            +this->t3_xmeam[a]*shp[2]+this->t4_xmeam[a]*shp[3]
            +this->t5_xmeam[a]*shp[4])/(rho0*rho0);
      Gbar = G_gam(gam, this->ibar_xmeam[a], errorflag);
    }


    this->rho_ref_xmeam[a] = rho0*Gbar;
  }
}

//------------------------------------------------------------------------------c
// Average weighting factors for the reference structure
void
XMEAM::get_tavref(double* t11av, double* t21av, double* t31av, double* t41av,
                  double* t51av, double* t12av, double* t22av, double* t32av,
                  double* t42av, double* t52av, double t11, double t21,
                  double t31, double t41, double t51, double t12, double t22,
                  double t32, double t42, double t52, double r, int a, int b,
                  lattice_t latt) {
  double rhoa01, rhoa02, a1, a2, rho01 /*,rho02*/;

  //     For ialloy = 2, no averaging is done
  if (this->ialloy == 2) {
    *t11av = t11;
    *t21av = t21;
    *t31av = t31;
    *t41av = t41;
    *t51av = t51;
    *t12av = t12;
    *t22av = t22;
    *t32av = t32;
    *t42av = t42;
    *t52av = t52;
  } else if (latt == FCC || latt == BCC || latt == DIA || latt == HCP
                         || latt == B1 || latt == DIM || latt == B2) {
    //     all neighbors are of the opposite type
    *t11av = t12;
    *t21av = t22;
    *t31av = t32;
    *t41av = t42;
    *t51av = t52;
    *t12av = t11;
    *t22av = t21;
    *t32av = t31;
    *t42av = t41;
    *t52av = t51;
  } else {
    a1 = r/this->re_xmeam[a][a]-1.0;
    a2 = r/this->re_xmeam[b][b]-1.0;
    rhoa01 = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta0_xmeam[a]*a1);
    rhoa02 = this->rho0_xmeam[b]*MathSpecial::fm_exp(-this->beta0_xmeam[b]*a2);
    if (latt == L12) {
      rho01 = 8*rhoa01+4*rhoa02;
      *t11av = (8*t11*rhoa01+4*t12*rhoa02)/rho01;
      *t12av = t11;
      *t21av = (8*t21*rhoa01+4*t22*rhoa02)/rho01;
      *t22av = t21;
      *t31av = (8*t31*rhoa01+4*t32*rhoa02)/rho01;
      *t32av = t31;
      *t41av = (8*t41*rhoa01+4*t42*rhoa02)/rho01;
      *t42av = t41;
      *t51av = (8*t51*rhoa01+4*t52*rhoa02)/rho01;
      *t52av = t51;
    } else {
      //      call error('Lattice not defined in get_tavref.')
    }
  }
}

//------------------------------------------------------------------------------c
void
XMEAM::get_sijk_rho(double C, int i, int j, int k, double* sijk) {
  double x;
  x = (C-this->Cmin_xmeam_rho[i][j][k])/(this->Cmax_xmeam_rho[i][j][k]
                                          -this->Cmin_xmeam_rho[i][j][k]);
  *sijk = fcut(x);
}


//------------------------------------------------------------------------------c
void
XMEAM::get_sijk_pair(double C, int i, int j, int k, double* sijk) {
  double x;
  x = (C-this->Cmin_xmeam_pair[i][j][k])/(this->Cmax_xmeam_pair[i][j][k]
                                     -this->Cmin_xmeam_pair[i][j][k]);
  *sijk = fcut(x);
}

//------------------------------------------------------------------------------c
// Calculate density functions, assuming reference configuration
void
XMEAM::get_densref(double r, int a, int b, double* rho01, double* rho11,
                   double* rho21, double* rho31, double* rho41, double* rho51,
                   double* rho02, double* rho12, double* rho22, double* rho32,
                   double* rho42, double* rho52) {
  double a1, a2;
  double s[5];
  lattice_t lat;
  int Zij2nn, Zij3nn, Zij4nn;
  double rhoa01nn, rhoa02nn;
  double rhoa01, rhoa11, rhoa21, rhoa31, rhoa41, rhoa51;
  double rhoa02, rhoa12, rhoa22, rhoa32, rhoa42, rhoa52;
  double arat, scrn, denom;
  double C, s111, s112, s221, S11, S22;

  a1 = r/this->re_xmeam[a][a]-1.0;
  a2 = r/this->re_xmeam[b][b]-1.0;

  rhoa01 = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta0_xmeam[a]*a1);
  rhoa11 = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta1_xmeam[a]*a1);
  rhoa21 = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta2_xmeam[a]*a1);
  rhoa31 = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta3_xmeam[a]*a1);
  rhoa41 = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta4_xmeam[a]*a1);
  rhoa51 = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta5_xmeam[a]*a1);
  rhoa02 = this->rho0_xmeam[b]*MathSpecial::fm_exp(-this->beta0_xmeam[b]*a2);
  rhoa12 = this->rho0_xmeam[b]*MathSpecial::fm_exp(-this->beta1_xmeam[b]*a2);
  rhoa22 = this->rho0_xmeam[b]*MathSpecial::fm_exp(-this->beta2_xmeam[b]*a2);
  rhoa32 = this->rho0_xmeam[b]*MathSpecial::fm_exp(-this->beta3_xmeam[b]*a2);
  rhoa42 = this->rho0_xmeam[b]*MathSpecial::fm_exp(-this->beta4_xmeam[b]*a2);
  rhoa52 = this->rho0_xmeam[b]*MathSpecial::fm_exp(-this->beta5_xmeam[b]*a2);

  lat = this->lattce_xmeam[a][b];

  *rho11 = *rho21 = *rho31 = *rho41 = *rho51 = 0.0;
  *rho12 = *rho22 = *rho32 = *rho42 = *rho52 = 0.0;

  if (lat == FCC) {
    *rho01 = 12.0*rhoa02;
    *rho02 = 12.0*rhoa01;
    *rho41 = 1.2*rhoa42*rhoa42;
    *rho42 = 1.2*rhoa41*rhoa41;
  } else if (lat == BCC) {
    *rho01 = 8.0*rhoa02;
    *rho02 = 8.0*rhoa01;
    *rho41 = 3.7926*rhoa42*rhoa42;
    *rho42 = 3.7926*rhoa41*rhoa41;
  } else if (lat == B1) {
    *rho01 = 6.0*rhoa02;
    *rho02 = 6.0*rhoa01;
  } else if (lat == DIA) {
    *rho01 = 4.0*rhoa02;
    *rho02 = 4.0*rhoa01;
    *rho31 = 32.0/9.0*rhoa32*rhoa32;
    *rho32 = 32.0/9.0*rhoa31*rhoa31;
    *rho41 = 92.0/15.0*rhoa42*rhoa42;
    *rho42 = 92.0/15.0*rhoa41*rhoa41;
  } else if (lat == HCP) {
		compute_symm_factors_nns(a,s);
		*rho01 = 12*rhoa02;
    *rho02 = 12*rhoa01;
		*rho11 = s[0]*rhoa12*rhoa12;
		*rho12 = s[0]*rhoa11*rhoa11;
		*rho21 = s[1]*rhoa22*rhoa22;
		*rho22 = s[1]*rhoa21*rhoa21;
    *rho31 = s[2]*rhoa32*rhoa32;
    *rho32 = s[2]*rhoa31*rhoa31;
    *rho41 = s[3]*rhoa42*rhoa42;
    *rho42 = s[3]*rhoa41*rhoa41;
    *rho51 = s[4]*rhoa52*rhoa52;
    *rho52 = s[4]*rhoa51*rhoa51;

  } else if (lat == DIM) {
    get_shpfcn(DIM, s);
    *rho01 = rhoa02;
    *rho02 = rhoa01;
    *rho11 = s[0]*rhoa12*rhoa12;
    *rho12 = s[0]*rhoa11*rhoa11;
    *rho21 = s[1]*rhoa22*rhoa22;
    *rho22 = s[1]*rhoa21*rhoa21;
    *rho31 = s[2]*rhoa32*rhoa32;
    *rho32 = s[2]*rhoa31*rhoa31;
    *rho41 = s[3]*rhoa42*rhoa42;
    *rho42 = s[3]*rhoa41*rhoa41;
    *rho51 = s[4]*rhoa52*rhoa52;
    *rho52 = s[4]*rhoa51*rhoa51;

  } else if (lat == C11) {
    *rho01 = rhoa01;
    *rho02 = rhoa02;
    *rho11 = rhoa11;
    *rho12 = rhoa12;
    *rho21 = rhoa21;
    *rho22 = rhoa22;
    *rho31 = rhoa31;
    *rho32 = rhoa32;
    *rho41 = rhoa41;
    *rho42 = rhoa42;
    *rho51 = rhoa51;
    *rho52 = rhoa52;
  } else if (lat == L12) {
    *rho01 = 8*rhoa01+4*rhoa02;
    *rho02 = 12*rhoa01;
    if (this->ialloy == 1) {
      *rho21 = 8.0/3.0*MathSpecial::square(rhoa21*this->t2_xmeam[a]
                                           -rhoa22*this->t2_xmeam[b]);
      denom = 8*rhoa01*MathSpecial::square(this->t2_xmeam[a])
             +4*rhoa02*MathSpecial::square(this->t2_xmeam[b]);
      if (denom > 0.)
        *rho21 = *rho21/denom*(*rho01);
    } else
      *rho21 = 8.0/3.0*(rhoa21-rhoa22)*(rhoa21-rhoa22);
  } else if (lat == B2) {

    *rho01 = 8.0*rhoa02;
    *rho02 = 8.0*rhoa01;
		*rho41 = 3.7926*rhoa42*rhoa42;
    *rho42 = 3.7926*rhoa41*rhoa41;
  } else {
    //        call error('Lattice not defined in get_densref.')
  }

  if (this->nn2_xmeam[a][b] == 1) {

    Zij2nn = get_Zij2(lat, this->Cmin_xmeam_rho[a][a][b],
											this->Cmax_xmeam_rho[a][a][b], this->re_xmeam[a][b],
                      this->rc_xmeam, this->delr_xmeam, arat, scrn);

    a1 = arat*r/this->re_xmeam[a][a]-1.0;
    a2 = arat*r/this->re_xmeam[b][b]-1.0;

    rhoa01nn = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta0_xmeam[a]*a1);
    rhoa02nn = this->rho0_xmeam[b]*MathSpecial::fm_exp(-this->beta0_xmeam[b]*a2);

    if (lat == L12) {
      //     As usual, L12 thinks it's special; we need to be careful computing
      //     the screening functions
      C = 1.0;
      get_sijk_rho(C, a, a, a, &s111);
      get_sijk_rho(C, a, a, b, &s112);
      get_sijk_rho(C, b, b, a, &s221);
      S11 = s111*s111*s112*s112;
      S22 = s221*s221*s221*s221;
      *rho01 = *rho01+6*S11*rhoa01nn;
      *rho02 = *rho02+6*S22*rhoa02nn;

    } else {
      //     For other cases, assume that second neighbor is of same type,
      //     first neighbor may be of different type

      *rho01 = *rho01+Zij2nn*scrn*rhoa01nn;

      //     Assume Zij2nn and arat don't depend on order, but scrn might
      Zij2nn = get_Zij2(lat, this->Cmin_xmeam_rho[b][b][a],
												this->Cmax_xmeam_rho[b][b][a], this->re_xmeam[b][a],
                        this->rc_xmeam, this->delr_xmeam, arat, scrn);
      *rho02 = *rho02+Zij2nn*scrn*rhoa02nn;
    }
  }

	if (this->nn4_xmeam[a][b] == 1) {
		Zij3nn = get_Zij3(lat, this->Cmin_xmeam_rho[a][a][b],
											this->Cmax_xmeam_rho[a][a][b], this->re_xmeam[a][b],
											this->rc_xmeam, this->delr_xmeam, arat, scrn);
		a1 = arat*r/this->re_xmeam[a][a]-1;
		a2 = arat*r/this->re_xmeam[b][b]-1;
		rhoa01nn = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta0_xmeam[a]*a1);
		rhoa02nn = this->rho0_xmeam[b]*MathSpecial::fm_exp(-this->beta0_xmeam[b]*a2);
		*rho01 = *rho01+Zij3nn*scrn*rhoa01nn;
    // Though scrn may be different by the order, but for pure HCP, it is the same.
		Zij3nn = get_Zij3(lat, this->Cmin_xmeam_rho[b][b][a],
											this->Cmax_xmeam_rho[b][b][a], this->re_xmeam[b][a],
											this->rc_xmeam, this->delr_xmeam, arat, scrn);
		*rho02 = *rho02+Zij3nn*scrn*rhoa02nn;

		Zij4nn = get_Zij4(lat, this->Cmin_xmeam_rho[a][a][b],
											this->Cmax_xmeam_rho[a][a][b], this->re_xmeam[a][b],
											this->rc_xmeam, this->delr_xmeam, arat, scrn);
		a1 = arat*r/this->re_xmeam[a][a]-1;
		a2 = arat*r/this->re_xmeam[b][b]-1;
		rhoa01nn = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta0_xmeam[a]*a1);
		rhoa02nn = this->rho0_xmeam[b]*MathSpecial::fm_exp(-this->beta0_xmeam[b]*a2);
		*rho01 = *rho01+Zij4nn*scrn*rhoa01nn;
    // Though scrn may be different by the order, but for pure HCP, it is the same.
		Zij4nn = get_Zij4(lat, this->Cmin_xmeam_rho[b][b][a],
											this->Cmax_xmeam_rho[b][b][a], this->re_xmeam[b][a],
											this->rc_xmeam, this->delr_xmeam, arat, scrn);
		*rho02 = *rho02+Zij4nn*scrn*rhoa02nn;
	}

}
//------------------------------------------------------------------------------
// Compute symmetric factors for the reference structure in 2NN case or 4NN case
//------------------------------------------------------------------------------
void
XMEAM::compute_symm_factors_nns(int a, double (&s)[5]) {
  double arat2 = 0, arat3 = 0, arat4 = 0;
	double scrn2 = 0,  scrn3 = 0, scrn4 = 0;
  double rho0_1nn = 0, rho1_1nn = 0, rho2_1nn = 0, rho3_1nn = 0, rho4_1nn = 0, rho5_1nn = 0;
  double rho0_2nn = 0, rho1_2nn = 0, rho2_2nn = 0, rho3_2nn = 0, rho4_2nn = 0, rho5_2nn = 0;
  double rho0_3nn = 0, rho1_3nn = 0, rho2_3nn = 0, rho3_3nn = 0, rho4_3nn = 0, rho5_3nn = 0;
  double rho0_4nn = 0, rho1_4nn = 0, rho2_4nn = 0, rho3_4nn = 0, rho4_4nn = 0, rho5_4nn = 0;
	int Z1 = 12, Z2 = 0, Z3 = 0, Z4 = 0;
	double symm_tmp1,symm_tmp2,symm_tmp3;
  double symm1 = 0.0, symm2 = 0.0, symm3 = 0.0, symm4 = 0.0, symm5 = 0.0;
  int i, j, k, m, n;
  int num1;
	double c = sqrt(8)/sqrt(3);

	double atoms_1nn[12][3] = {{1.0,0,0},{1.0/2.0,sqrt(3)/2,0},{-1.0/2.0,sqrt(3)/2,0},{-1.0,0,0},{-1.0/2.0,-sqrt(3)/2,0},{1.0/2.0,-sqrt(3)/2,0},{1.0/2.0,sqrt(3)/6,c/2.0},{-1.0/2.0,sqrt(3)/6,c/2.0},{0,-sqrt(3)/3,c/2.0},{1.0/2.0,sqrt(3)/6,-c/2.0},{-1.0/2.0,sqrt(3)/6,-c/2.0},{0,-sqrt(3)/3,-c/2.0}};
	double atoms_2nn[6][3] = {{-1.0,-sqrt(3)/3,c/2},{1.0,-sqrt(3)/3,c/2},{0,2*sqrt(3)/3,c/2},{-1.0,-sqrt(3)/3,-c/2},{1.0,-sqrt(3)/3,-c/2},{0,2*sqrt(3)/3,-c/2}};
	double atoms_3nn[2][3] = {{0,0,c},{0,0,-c}};
	double atoms_4nn[18][3] = {{3.0/2.0,sqrt(3)/2,0},{0,sqrt(3),0},{-3.0/2.0,sqrt(3)/2,0},{-3.0/2.0,-sqrt(3)/2,0},{0,-sqrt(3),0},{3.0/2.0,-sqrt(3)/2,0},{3.0/2.0,sqrt(3)/6,c/2},{1.0,sqrt(3)/3*2,c/2},{-1.0,sqrt(3)/3*2,c/2},{-3.0/2.0,sqrt(3)/6,c/2},{-1.0/2.0,-sqrt(3)*5/6,c/2},{1.0/2.0,-sqrt(3)*5/6,c/2},{3.0/2.0,sqrt(3)/6,-c/2},{1.0,sqrt(3)/3*2,-c/2},{-1.0,sqrt(3)/3*2,-c/2},{-3.0/2.0,sqrt(3)/6,-c/2},{-1.0/2.0,-sqrt(3)*5/6,-c/2},{1.0/2.0,-sqrt(3)*5/6,-c/2}};

	switch (this->lattce_xmeam[a][a]) {
	case HCP:
		s[0] = s[1] = s[2] = s[3] = s[4] = 0;
		rho0_1nn = this->rho0_xmeam[a];
		rho1_1nn = this->rho0_xmeam[a];
		rho2_1nn = this->rho0_xmeam[a];
		rho3_1nn = this->rho0_xmeam[a];
		rho4_1nn = this->rho0_xmeam[a];
		rho5_1nn = this->rho0_xmeam[a];

		if (this->nn2_xmeam[a][a]==1) {
			Z2 = get_Zij2(this->lattce_xmeam[a][a], this->Cmin_xmeam_rho[a][a][a],
										this->Cmax_xmeam_rho[a][a][a], this->re_xmeam[a][a],
										this->rc_xmeam, this->delr_xmeam, arat2, scrn2);
			rho0_2nn = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta0_xmeam[a]*(arat2-1));
			rho1_2nn = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta1_xmeam[a]*(arat2-1));
			rho2_2nn = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta2_xmeam[a]*(arat2-1));
			rho3_2nn = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta3_xmeam[a]*(arat2-1));
			rho4_2nn = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta4_xmeam[a]*(arat2-1));
			rho5_2nn = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta5_xmeam[a]*(arat2-1));
			//rho0_2nn = rho1_2nn = rho2_2nn = rho3_2nn = rho4_2nn = rho5_2nn = 0.3;
		}
		if (this->nn4_xmeam[a][a]==1) {
			Z3 = get_Zij3(this->lattce_xmeam[a][a], this->Cmin_xmeam_rho[a][a][a],
										this->Cmax_xmeam_rho[a][a][a], this->re_xmeam[a][a],
										this->rc_xmeam, this->delr_xmeam, arat3, scrn3);
			Z4 = get_Zij4(this->lattce_xmeam[a][a], this->Cmin_xmeam_rho[a][a][a],
										this->Cmax_xmeam_rho[a][a][a], this->re_xmeam[a][a],
										this->rc_xmeam, this->delr_xmeam, arat4, scrn4);
			rho0_3nn = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta0_xmeam[a]*(arat3-1));
			rho1_3nn = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta1_xmeam[a]*(arat3-1));
			rho2_3nn = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta2_xmeam[a]*(arat3-1));
			rho3_3nn = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta3_xmeam[a]*(arat3-1));
			rho4_3nn = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta4_xmeam[a]*(arat3-1));
			rho5_3nn = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta5_xmeam[a]*(arat3-1));

			rho0_4nn = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta0_xmeam[a]*(arat4-1));
			rho1_4nn = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta1_xmeam[a]*(arat4-1));
			rho2_4nn = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta2_xmeam[a]*(arat4-1));
			rho3_4nn = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta3_xmeam[a]*(arat4-1));
			rho4_4nn = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta4_xmeam[a]*(arat4-1));
			rho5_4nn = this->rho0_xmeam[a]*MathSpecial::fm_exp(-this->beta5_xmeam[a]*(arat4-1));
			//rho0_3nn = rho1_3nn = rho2_3nn = rho3_3nn = rho4_3nn = rho5_3nn = 0.2;
			//rho0_4nn = rho1_4nn = rho2_4nn = rho3_4nn = rho4_4nn = rho5_4nn = 0.2;
		}

		for (i = 0; i < 3; ++i) {
			symm_tmp1 = 0;
			symm_tmp2 = 0;
			symm_tmp3 = 0;
			for (num1 = 0; num1 < Z1; ++num1) {
				symm_tmp1 += atoms_1nn[num1][i]*rho1_1nn;
				symm_tmp2 += atoms_1nn[num1][i]*rho3_1nn;
				symm_tmp3 += atoms_1nn[num1][i]*rho5_1nn;
			}
			for (num1 = 0; num1 < Z2; ++num1) {
				symm_tmp1 += atoms_2nn[num1][i]*rho1_2nn*scrn2;
				symm_tmp2 += atoms_2nn[num1][i]*rho3_2nn*scrn2;
				symm_tmp3 += atoms_2nn[num1][i]*rho5_2nn*scrn2;
			}
			for (num1 = 0; num1 < Z3; ++num1) {
				symm_tmp1 += atoms_3nn[num1][i]*rho1_3nn*scrn3;
				symm_tmp2 += atoms_3nn[num1][i]*rho3_3nn*scrn3;
				symm_tmp3 += atoms_3nn[num1][i]*rho5_3nn*scrn3;
			}
			for (num1 = 0; num1 < Z4; ++num1) {
				symm_tmp1 += atoms_4nn[num1][i]*rho1_4nn*scrn4;
				symm_tmp2 += atoms_4nn[num1][i]*rho3_4nn*scrn4;
				symm_tmp3 += atoms_4nn[num1][i]*rho5_4nn*scrn4;
			}
			symm1 += symm_tmp1*symm_tmp1;
			symm3 -= 3.0/5.0*symm_tmp2*symm_tmp2;
			symm5 += 5.0/21.0*symm_tmp3*symm_tmp3;
			for (j = 0; j < 3; ++j) {
				symm_tmp1 = 0;
				symm_tmp2 = 0;
				for (num1 = 0; num1 < Z1; ++num1) {
					symm_tmp1 += atoms_1nn[num1][i]*atoms_1nn[num1][j]*rho2_1nn;
					symm_tmp2 += atoms_1nn[num1][i]*atoms_1nn[num1][j]*rho4_1nn;
				}
				for (num1 = 0; num1 < Z2; ++num1) {
					symm_tmp1 += atoms_2nn[num1][i]*atoms_2nn[num1][j]*rho2_2nn*scrn2;
					symm_tmp2 += atoms_2nn[num1][i]*atoms_2nn[num1][j]*rho4_2nn*scrn2;
				}
				for (num1 = 0; num1 < Z3; ++num1) {
					symm_tmp1 += atoms_3nn[num1][i]*atoms_3nn[num1][j]*rho2_3nn*scrn3;
					symm_tmp2 += atoms_3nn[num1][i]*atoms_3nn[num1][j]*rho2_4nn*scrn3;
				}

				for (num1 = 0; num1 < Z4; ++num1) {
					symm_tmp1 += atoms_4nn[num1][i]*atoms_4nn[num1][j]*rho2_4nn*scrn4;
					symm_tmp2 += atoms_4nn[num1][i]*atoms_4nn[num1][j]*rho4_4nn*scrn4;
				}
				symm2 += symm_tmp1*symm_tmp1;
				symm4 -= 6.0/7.0*symm_tmp2*symm_tmp2;
				for (k = 0; k < 3; ++k) {
					symm_tmp1 = 0;
					symm_tmp2 = 0;
					for (num1 = 0; num1 < Z1; ++num1) {
						symm_tmp1 += atoms_1nn[num1][i]*atoms_1nn[num1][j]*atoms_1nn[num1][k]*rho3_1nn;
						symm_tmp2 += atoms_1nn[num1][i]*atoms_1nn[num1][j]*atoms_1nn[num1][k]*rho5_1nn;
					}
					for (num1 = 0; num1 < Z2; ++num1) {
						symm_tmp1 += atoms_2nn[num1][i]*atoms_2nn[num1][j]*atoms_2nn[num1][k]*rho3_2nn*scrn2;
						symm_tmp2 += atoms_2nn[num1][i]*atoms_2nn[num1][j]*atoms_2nn[num1][k]*rho5_2nn*scrn2;
					}
					for (num1 = 0; num1 < Z3; ++num1) {
						symm_tmp1 += atoms_3nn[num1][i]*atoms_3nn[num1][j]*atoms_3nn[num1][k]*rho3_3nn*scrn3;
						symm_tmp2 += atoms_3nn[num1][i]*atoms_3nn[num1][j]*atoms_3nn[num1][k]*rho5_3nn*scrn3;
					}
					for (num1 = 0; num1 < Z4; ++num1) {
						symm_tmp1 += atoms_4nn[num1][i]*atoms_4nn[num1][j]*atoms_4nn[num1][k]*rho3_4nn*scrn4;
						symm_tmp2 += atoms_4nn[num1][i]*atoms_4nn[num1][j]*atoms_4nn[num1][k]*rho5_4nn*scrn4;
					}
					symm3 += symm_tmp1*symm_tmp1;
					symm5 -= 10.0/9.0*symm_tmp2*symm_tmp2;
					for (m = 0; m < 3; ++m) {
						symm_tmp1 = 0;
						for (num1 = 0; num1 < Z1; ++num1) {
							symm_tmp1 += atoms_1nn[num1][i]*atoms_1nn[num1][j]*atoms_1nn[num1][k]*atoms_1nn[num1][m]*rho4_1nn;
						}
						for (num1 = 0; num1 < Z2; ++num1) {
							symm_tmp1 += atoms_2nn[num1][i]*atoms_2nn[num1][j]*atoms_2nn[num1][k]*atoms_2nn[num1][m]*rho4_2nn*scrn2;
						}
						for (num1 = 0; num1 < Z3; ++num1) {
							symm_tmp1 += atoms_3nn[num1][i]*atoms_3nn[num1][j]*atoms_3nn[num1][k]*atoms_3nn[num1][m]*rho4_3nn*scrn3;
						}
						for (num1 = 0; num1 < Z4; ++num1) {
							symm_tmp1 += atoms_4nn[num1][i]*atoms_4nn[num1][j]*atoms_4nn[num1][k]*atoms_4nn[num1][m]*rho4_4nn*scrn4;
						}
						symm4 += symm_tmp1*symm_tmp1;

						for (n = 0; n < 3; ++n) {
							symm_tmp1 = 0;
							for (num1 = 0; num1 < Z1; ++num1) {
								symm_tmp1 += atoms_1nn[num1][i]*atoms_1nn[num1][j]*atoms_1nn[num1][k]*atoms_1nn[num1][m]*atoms_1nn[num1][n]*rho5_1nn;
							}
							for (num1 = 0; num1 < Z2; ++num1) {
								symm_tmp1 += atoms_2nn[num1][i]*atoms_2nn[num1][j]*atoms_2nn[num1][k]*atoms_2nn[num1][m]*atoms_2nn[num1][n]*rho5_2nn*scrn2;
							}
							for (num1 = 0; num1 < Z3; ++num1) {
								symm_tmp1 += atoms_3nn[num1][i]*atoms_3nn[num1][j]*atoms_3nn[num1][k]*atoms_3nn[num1][m]*atoms_3nn[num1][n]*rho5_3nn*scrn3;
							}
							for (num1 = 0; num1 < Z4; ++num1) {
								symm_tmp1 += atoms_4nn[num1][i]*atoms_4nn[num1][j]*atoms_4nn[num1][k]*atoms_4nn[num1][m]*atoms_4nn[num1][n]*rho5_4nn*scrn4;
							}
							symm5 += symm_tmp1*symm_tmp1;
						}
					}
				}
			}
		}
		symm_tmp1 = Z1*rho2_1nn+Z2*rho2_2nn*scrn2+Z3*rho2_3nn*scrn3+Z4*rho2_4nn*scrn4;
		symm2 = symm2 - (symm_tmp1)*(symm_tmp1)/3;
		symm_tmp1 = Z1*rho4_1nn+Z2*rho4_2nn*scrn2+Z3*rho4_3nn*scrn3+Z4*rho4_4nn*scrn4;
		symm4 += 3.0/35.0*symm_tmp1*symm_tmp1;
		s[0] = symm1;
		s[1] = symm2;
		s[2] = symm3;
		s[3] = symm4;
		s[4] = symm5;
		break;
	default:
		get_shpfcn(this->lattce_xmeam[a][a],s);
  }
}

void
XMEAM::interpolate_xmeam(int ind)
{
  int j;
  double drar;

  // map to coefficient space

  this->nrar = this->nr;
  drar = this->dr;
  this->rdrar = 1.0/drar;

  // phir interp
  for (j = 0; j < this->nrar; j++) {
    this->phirar[ind][j] = this->phir[ind][j];
  }
  this->phirar1[ind][0] = this->phirar[ind][1]-this->phirar[ind][0];
  this->phirar1[ind][1] = 0.5*(this->phirar[ind][2]-this->phirar[ind][0]);
  this->phirar1[ind][this->nrar-2] = 0.5*(this->phirar[ind][this->nrar-1]
                                         -this->phirar[ind][this->nrar-3]);
  this->phirar1[ind][this->nrar-1] = 0.0;
  for (j = 2; j < this->nrar - 2; j++) {
    this->phirar1[ind][j] = ((this->phirar[ind][j-2]-this->phirar[ind][j+2])
                  +8.0*(this->phirar[ind][j+1]-this->phirar[ind][j-1]))/12.0;
  }

  for (j = 0; j < this->nrar - 1; j++) {
    this->phirar2[ind][j] = 3.0*(this->phirar[ind][j+1]-this->phirar[ind][j])
                          -2.0*this->phirar1[ind][j]-this->phirar1[ind][j+1];
    this->phirar3[ind][j] = this->phirar1[ind][j]+this->phirar1[ind][j+1]
                          -2.0*(this->phirar[ind][j+1]-this->phirar[ind][j]);
  }
  this->phirar2[ind][this->nrar-1] = 0.0;
  this->phirar3[ind][this->nrar-1] = 0.0;

  for (j = 0; j < this->nrar; j++) {
    this->phirar4[ind][j] = this->phirar1[ind][j]/drar;
    this->phirar5[ind][j] = 2.0*this->phirar2[ind][j]/drar;
    this->phirar6[ind][j] = 3.0*this->phirar3[ind][j]/drar;
  }
}
